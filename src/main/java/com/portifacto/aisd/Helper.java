package com.portifacto.aisd;

public class Helper {

    // sort algorithms

    double[] makeCopy(double[] data, int size) {
        double[] result = new double[size];
        for (int i = 0; i < size; i++) {
            result[i] = data[i];
        }
        return result;
    }

    void swap(double[] data, int i, int j) {
        double x = data[i];
        data[i] = data[j];
        data[j] = x;
    }

    public static String toString(double[] data, int size) {
        StringBuilder sb = new StringBuilder();
        sb.append('[');
        for (int i = 0; i < size; i++) {
            sb.append(data[i]);
            sb.append(',');
        }
        sb.append(']');
        return sb.toString();
    }

    // graph algorithms

    protected int findUnvisited(boolean[] visited) {
        for (int i = 0; i < visited.length; i++) {
            if (!visited[i]) {
                return i;
            }
        }
        return -1;
    }

    // for graph algorithms

    protected boolean hasBackEdge(IGraph graph, int rootIndex,
                                  boolean[] visited, boolean[] path) {
        if (path[rootIndex]) return true;
        if (visited[rootIndex]) return false;

        path[rootIndex] = true;
        visited[rootIndex] = true;

        for (int i = 0; i < graph.getNodeCount(); i++) {
            if (graph.isEdgePresent(rootIndex, i)) {
                if (hasBackEdge(graph, i, visited, path)) return true;
            }
        }
        path[rootIndex] = false;
        return false;
    }


    // list pretty printer
    protected String listPrettyPrint(IList list) {
        return listPrettyPrint(list.iterator());
    }

    protected String listPrettyPrint(IIterator iterator) {
        StringBuilder sb = new StringBuilder();
        Double d;
        sb.append('[');
        while ((d = iterator.get()) != null) {
            sb.append(d);
            sb.append(',');
        }
        sb.append(']');
        return sb.toString();
    }


}


