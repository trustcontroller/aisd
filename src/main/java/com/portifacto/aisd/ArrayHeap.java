package com.portifacto.aisd;

import java.util.function.Function;

/**
 * Contract/guarantees:
 * 1. Binary heap, array implementation
 * 2. Unbound
 * 3. Max heap or min heap, set at construction time
 * 4. BFS iterator as standard iterator
 * 5. Duplicates allowed, no guarantee on their order (no stability)
 * 6. Sorting is performed in-place
 */
public class ArrayHeap extends ExpandableStorage implements IOrderedTree, IQueue, ISort {

    /*
     * Heap is implemented here as an array structure that stores the
     * complete binary tree
     *
     *  | top of the heap
     *  0  1  2  3  4  5  6  7  8  9  10
     *           | for the element at index i=3:
     *     | iParent(i)  = (i-1) / 2  -> (3-1)/2 -> 1
     *                       | iLeftChild(i)  = 2*i + 1 -> 2*3+1 -> 7
     *                          | iRightChild(i) = 2*i + 2 -> 2*3+2 -> 8
     * or graphically
     *               _________________
     *   _____    __|___________   |  |
     *  |  V  V  |  |        V  V  V  V
     *  0  1  2  3  4  5  6  7  8  9  10
     *     |__|_ A__A  A  A
     *        |________|__|
     */

    public enum Order {
        MAX_HEAP,
        MIN_HEAP
    }

    enum Traversal {
        BREADTH_FIRST,
        DEPTH_FIRST_NLR,
        DEPTH_FIRST_LNR,
        DEPTH_FIRST_LRN,
    }


    private int occupied = 0;
    private final Order order;

    public ArrayHeap(int capacity, Order order) {
        super(capacity);
        this.order = order;
    }

    public ArrayHeap(Order order) {
        super();
        this.order = order;
    }

    public ArrayHeap() {
        super();
        order = Order.MAX_HEAP;
    }

    @Override
    public boolean add(double value) {
        if (occupied >= storage.length) incrementCapacity();
        storage[occupied++] = value;
        if (order == Order.MAX_HEAP) toMaxHeap();
        else toMinHeap();
        return true;
    }

    @Override
    public boolean find(double value) {
        for (int i = 0; i < occupied; i++)
            if (storage[i] == value) return true;
        return false;
    }

    @Override
    public boolean isEmpty() {
        return occupied == 0;
    }

    @Override
    public int size() {
        return occupied;
    }

    @Override
    public int capacity() {
        return super.capacity();
    }

    @Override
    public boolean offer(double value) {
        return add(value);
    }

    @Override
    public Double poll() {
        if (occupied == 0) return null;
        double result = storage[0];
        storage[0] = storage[occupied - 1];
        occupied--;
        if (order == Order.MAX_HEAP) toMaxHeap();
        else toMinHeap();
        return result;
    }

    @Override
    public double[] sort(double[] data, int size) {
        if (data == null || size < 0 || size > data.length) throw new IllegalArgumentException();
        for (int i = 0; i < size; i++) add(data[i]);
        if (order == Order.MAX_HEAP) toMaxHeap();
        else toMinHeap();
        for (int i = size - 1; i > 0; i--) {
            swap(storage, 0, i); // the highest went to the end
            if (order == Order.MAX_HEAP) heapify(0, i - 1, x -> x > 0);
            else heapify(0, i - 1, x -> x < 0);
        }
        return makeCopy(storage, occupied);
    }

    @Override
    public IIterator lrnIterator() {
        IList list = new LinkedList();
        if (isEmpty()) return list.iterator();
        visit(list, 0, Traversal.DEPTH_FIRST_LRN);
        return list.iterator();
    }

    @Override
    public IIterator lnrIterator() {
        IList list = new LinkedList();
        if (isEmpty()) return list.iterator();
        visit(list, 0, Traversal.DEPTH_FIRST_LNR);
        return list.iterator();
    }

    @Override
    public IIterator nlrIterator() {
        IList list = new LinkedList();
        if (isEmpty()) return list.iterator();
        visit(list, 0, Traversal.DEPTH_FIRST_NLR);
        return list.iterator();
    }

    private void visit(IList list, int parent, Traversal order) {
        if (parent >= occupied) return;
        int left = parent * 2 + 1;
        int right = parent * 2 + 2;
        switch (order) {
            case DEPTH_FIRST_LNR:
                visit(list, left, order);
                list.add(storage[parent]);
                visit(list, right, order);
                break;
            case DEPTH_FIRST_LRN:
                visit(list, left, order);
                visit(list, right, order);
                list.add(storage[parent]);
                break;
            case DEPTH_FIRST_NLR:
                list.add(storage[parent]);
                visit(list, left, order);
                visit(list, right, order);
                break;
        }
    }

    @Override
    public IIterator bfIterator() {
        return iterator();
    }

    @Override
    public IIterator iterator() {
        return new HeapIterator(storage, occupied);
    }

    private static class HeapIterator implements IIterator {

        private final double[] storage;
        private final int size;
        private int index = 0;

        private HeapIterator(double[] storage, int size) {
            this.storage = storage;
            this.size = size;
        }

        @Override
        public Double get() {
            if (index >= size) return null;
            return storage[index++];
        }
    }

    private void toMaxHeap() {
        for (int i = occupied - 1; i >= 0; i--) {
            heapify(i, occupied - 1, x -> x > 0);
        }
    }

    private void toMinHeap() {
        for (int i = occupied - 1; i >= 0; i--) {
            heapify(i, occupied - 1, x -> x < 0);
        }
    }

    /*
     * Repairs the heap at the parent assuming that its children are proper heaps.
     * Never exceeds the last index, so we can do in-place sort later on.
     * Operates by comparing parent and its two immediate children.
     * If in order, then nothing. If had to swap with one of children,
     * then this child has to be repaired recursively.
     */
    private void heapify(int parent, int last, Function<Integer, Boolean> order) {

        // first determine indexes of both children, if present
        int left = parent * 2 + 1;
        int right = parent * 2 + 2;

        // if both outside of last then we are done
        if (left > last && right > last) return;

        // if only right out of scope, the use parent and left
        if (right > last) {
            if (!order.apply(Double.compare(storage[parent], storage[left]))) {
                swap(storage, parent, left);
                heapify(left, last, order);
                return;
            }
            return;
        }

        // both children are in scope, find what is the largest element
        boolean pl = order.apply(Double.compare(storage[parent], storage[left]));
        boolean pr = order.apply(Double.compare(storage[parent], storage[right]));
        boolean lr = order.apply(Double.compare(storage[left], storage[right]));

        int max = parent;
        if (!pl) {
            max = left;
            if (!lr) max = right;
        } else if (!pr) {
            max = right;
            if (lr) max = left;
        }

        // if parent reached then done
        if (max == parent) return;
        // max is as the left
        if (max == left) {
            swap(storage, parent, left);
            heapify(left, last, order);
            return;
        }
        // max is at the right
        swap(storage, parent, right);
        heapify(right, last, order);
    }

}
