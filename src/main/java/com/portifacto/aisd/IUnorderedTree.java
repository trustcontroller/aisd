package com.portifacto.aisd;

/**
 * Contract/guarantees:
 * 1. Tree structure: root and nodes, no cycles
 * 2. Number of children: not limited, no children allowed
 * 3. Value: double
 * 4. Uniqueness: no, several nodes may hold the same value
 * 5. Order: as defined by the construction process
 * 6. Permanence: only appendage, no changes
 * 7. Iterator: depth-first, node value and then its children
 */

/**
 * This interface makes use of a token, that is passed from one call
 * to another. The token is opaque and passed as the object of a class Object.
 * Conceptually, the token is a reference to a node from this tree.
 */
public interface IUnorderedTree extends IIterable, ICollection {

    /**
     * Appends the node with a provided value to the empty tree.
     * The node will become a root node of the tree.
     *
     * @param value To be added in the node to the tree.
     * @return A token that represents a root node.
     * @throws IllegalArgumentException if the tree is not empty
     */
    public Object append(double value);

    /**
     * Appends the node to a node referenced by a token.
     *
     * @param node  The token that references the parent node
     * @param value To be added in the node to the tree.
     * @return A token that represents an added node.
     * @throws IllegalArgumentException if the token does not reference
     *                                  a node from this tree
     */
    public Object append(Object node, double value);

}


