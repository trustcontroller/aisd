package com.portifacto.aisd;

public interface IPermutationCallback {

    public void process(int[] permutation);

}
