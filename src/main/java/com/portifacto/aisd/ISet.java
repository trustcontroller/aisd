package com.portifacto.aisd;

/**
 * Contract/guarantees:
 * 1. Structure: set, repetitions not allowed
 * 3. Value: double
 * 4. Uniqueness: duplicates rejected
 * 5. Order: random, not guaranteed
 * 6. Permanence: addition only
 * 7. Iterator: no order guaranteed
 */

public interface ISet extends IIterable, ICollection {

    /**
     * Attempts to add the value to the set. Duplicate values are not allowed.
     *
     * @param value value to be added. Duplicate values are not allowed.
     * @return true if added, false otherwise. Attempt to add duplicate values
     * ends up in false.
     */
    public boolean add(double value);

    /**
     * Determines whether the set contains a value
     *
     * @param value Values to looked up in the set
     * @return true if there is a value, false otherwise
     */
    public boolean find(double value);

}
