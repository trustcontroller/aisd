package com.portifacto.aisd;

import java.util.BitSet;

/**
 * Does NOT implement ISort, integers only
 * Uses BitSet from java.utils
 */
public class PigeonholeSort {

    public int[] sort(int[] data) {
        if (data == null) throw new IllegalArgumentException();
        return sort(data, data.length);
    }

    public int[] sort(int[] data, int size) {
        if (size == 0) return new int[0];
        int min = Integer.MAX_VALUE;
        int max = Integer.MIN_VALUE;
        int[] local = new int[size];
        for (int i = 0; i < size; i++) {
            if (data[i] < min) min = data[i];
            if (data[i] > max) max = data[i];
            local[i] = data[i];
        }
        final int length = max - min + 1;
        BitSet temp = new BitSet(length);
        for (int i = 0; i < data.length; i++) temp.set(data[i] - min);
        for (int i = 0, k = 0; i < length; i++)
            if (temp.get(i)) local[k++] = min + i;
        return local;
    }
}


