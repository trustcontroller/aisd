package com.portifacto.aisd;

/**
 * Conventions:
 * null as data: not possible
 * duplicate values: allowed, stored separately, find or delete acts on any copy
 * order preservation: strict
 */
public class UnorderedArrayList extends ExpandableStorage implements IList {

    private int size = 0;

    public UnorderedArrayList(int capacity) {
        super(capacity);
    }

    public UnorderedArrayList() {
        super();
    }

    @Override
    public boolean add(double value) {
        if (size >= storage.length) incrementCapacity();
        storage[size++] = value;
        return true;
    }

    @Override
    public boolean find(double value) {
        for (int i = 0; i < size; i++)
            if (storage[i] == value) return true;
        return false;
    }

    @Override
    public boolean remove(double value) {
        for (int i = 0; i < size; i++)
            if (storage[i] == value) {
                for (int j = i; j < size - 1; j++)
                    storage[j] = storage[j + 1];
                size--;
                return true;
            }
        return false;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public int capacity() {
        return super.capacity();
    }

    @Override
    public IIterator iterator() {
        return new ArrayIterator(storage, size);
    }

    static class ArrayIterator implements IIterator {

        private final double[] storage;
        private final int size;
        private int index;

        ArrayIterator(double[] storage, int size) {
            this.storage = storage;
            this.size = size;
            this.index = 0;
        }

        public Double get() {
            if (size == 0) return null;
            if (index >= size) return null;
            return storage[index++];
        }
    }

    @Override
    public String toString() {
        return listPrettyPrint(this.iterator());
    }


}
