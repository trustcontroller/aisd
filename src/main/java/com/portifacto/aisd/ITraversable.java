package com.portifacto.aisd;

/**
 * Contract/guarantees:
 * 1. All elements of the ADS will be delivered by any iterator
 * 2. Each complete iteration exhausts the iterator
 * 3. Subsequent iterators delivers elements in the same order
 * provided that the ADS did not change
 * 4. The order of elements is defined in terms of traversing
 * (visiting) the binary tree
 * 5. The implementation of the iterator for this ADS may provide
 * an order that is different from any order of traversal,
 * or it can be equivalent to one of orders provided by the
 * traversal
 * 6. No guarantees in case the ADS change during iteration
 * 7. No thread safety
 */

public interface ITraversable extends IIterable {
    /**
     * Returns iterator for the order Left-Right-Node
     *
     * @return iterator
     */
    public IIterator lrnIterator();

    /**
     * Returns iterator for the order Left-Node-Right
     *
     * @return iterator
     */
    public IIterator lnrIterator();

    /**
     * Returns iterator for the order Node-Left-Right
     *
     * @return iterator
     */
    public IIterator nlrIterator();

    /**
     * Returns iterator for the breadth-first order
     *
     * @return iterator
     */
    public IIterator bfIterator();
}
