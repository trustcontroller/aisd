package com.portifacto.aisd;

public class NaiveArray implements IArray {

    private final static int DEFAULT_CAPACITY = 10;

    private final double[] storage;

    public NaiveArray() {
        this(DEFAULT_CAPACITY);
    }

    public NaiveArray(int capacity) {
        if (capacity < 0) throw new IllegalArgumentException();
        storage = new double[capacity];
    }

    @Override
    public double get(int index) {
        if (index < 0 || index >= storage.length)
            throw new ArrayIndexOutOfBoundsException(Integer.toString(index));
        return storage[index];
    }

    @Override
    public void set(int index, double value) {
        if (index < 0 || index >= storage.length)
            throw new ArrayIndexOutOfBoundsException(Integer.toString(index));
        storage[index] = value;
    }

    @Override
    public int capacity() {
        return storage.length;
    }

    @Override
    public int size() {
        return storage.length;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append('[');
        for (int i = 0; i < storage.length; i++) {
            sb.append(storage[i]);
            sb.append(',');
        }
        sb.append(']');
        return sb.toString();
    }


}
