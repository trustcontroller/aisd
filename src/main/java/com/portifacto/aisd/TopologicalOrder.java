package com.portifacto.aisd;

public class TopologicalOrder extends Helper {

    private final IGraph graph;

    public TopologicalOrder(IGraph graph) {
        if (graph == null) throw new IllegalArgumentException();
        this.graph = graph;
    }

    public String[] topologicalOrder() {
        boolean[] visited = new boolean[graph.getNodeCount()];
        boolean[] processed = new boolean[graph.getNodeCount()];
        String[] result = new String[graph.getNodeCount()];

        int index;
        int position = 0;
        while ((index = findUnvisited(visited)) >= 0) {
            processed[index] = true;
            position = topologicalOrder(index, visited, processed, result, position);
            processed[index] = false;
            if (position < 0) return null;
        }
        return result;
    }

    private int topologicalOrder(int index, boolean[] visited, boolean[] processed, String[] result, int position) {
        if (visited[index]) return position;
        for (int i = 0; i < graph.getNodeCount(); i++) {
            if (graph.isEdgePresent(index, i)) {
                if (processed[i]) return -1;
                processed[i] = true;
                position = topologicalOrder(i, visited, processed, result, position);
                processed[i] = false;
            }
        }
        visited[index] = true;
        // add first
        for (int i = result.length - 1; i > 0; i--) result[i] = result[i - 1];
        result[0] = graph.getNodeName(index);
        return position;
    }


}
