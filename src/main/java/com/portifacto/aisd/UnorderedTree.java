package com.portifacto.aisd;

public class UnorderedTree implements IUnorderedTree {

    public static class Node {
        UnorderedTree tree;
        double value;
        Node next; // this level
        Node head; // children
        Node tail;

        public Node(UnorderedTree tree, double value) {
            this.tree = tree;
            this.value = value;
            this.next = null;
            this.head = null;
            this.tail = null;
        }
    }

    public UnorderedTree() {
    }

    private Node root = null;

    private int size = 0;

    @Override
    public Object append(double value) {
        if (root != null)
            throw new IllegalArgumentException();
        Node alter = new Node(this, value);
        root = alter;
        size++;
        return alter;
    }

    @Override
    public Object append(Object node, double value) {
        if (!(node instanceof Node) || ((Node) node).tree != this)
            throw new IllegalArgumentException();
        Node alter = new Node(this, value);
        if (((Node) node).head == null) {
            ((Node) node).head = alter;
        } else {
            ((Node) node).tail.next = alter;
        }
        ((Node) node).tail = alter;
        size++;
        return alter;
    }

    @Override
    public int capacity() {
        return Integer.MAX_VALUE;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return root == null;
    }


    @Override
    public IIterator iterator() {
        IList list = new LinkedList();
        visitNode(list, root);
        return list.iterator();
    }

    private void visitNode(IList list, Node node) {
        if (node == null) return;
        list.add(node.value);
        for (Node current = node.head; current != null; current = current.next)
            visitNode(list, current);
    }
}
