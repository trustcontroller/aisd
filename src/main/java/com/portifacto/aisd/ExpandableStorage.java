package com.portifacto.aisd;

public class ExpandableStorage extends Helper {

    protected static int CAPACITY_INCREMENT = 10;

    protected double[] storage;

    public ExpandableStorage() {
        this(CAPACITY_INCREMENT);
    }

    public ExpandableStorage(int capacity) {
        if (capacity < 0) throw new IllegalArgumentException();
        storage = new double[capacity];
    }

    protected void incrementCapacity() {
        int capacity = storage.length + CAPACITY_INCREMENT;
        double[] newArray = new double[capacity];
        for (int i = 0; i < storage.length; i++) newArray[i] = storage[i];
        storage = newArray;
    }

    protected void incrementCapacity(int minimumRequired) {
        if (minimumRequired < 0) return;
        int newCapacity = ((minimumRequired + CAPACITY_INCREMENT) /
                CAPACITY_INCREMENT) * CAPACITY_INCREMENT;
        if (newCapacity > storage.length) {
            double[] newArray = new double[newCapacity];
            for (int i = 0; i < storage.length; i++) newArray[i] = storage[i];
            storage = newArray;
        }
    }

    protected int capacity() {
        return Integer.MAX_VALUE;
    }

    protected int length() {
        return storage.length;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append('[');
        for (int i = 0; i < storage.length; i++) {
            sb.append(storage[i]);
            sb.append(',');
        }
        sb.append(']');
        return sb.toString();
    }

}
