package com.portifacto.aisd;

/**
 * Contract/guarantees:
 * 1. Tree structure: root and nodes, no cycles
 * 2. Number of children: at the most two, left and right
 * 3. Value: double
 * 4. Uniqueness: no, several nodes may hold the same value
 * 5. Order: as defined by the construction process
 * 6. Permanence: only appendage, no changes
 * 7. Iterator: depth-first, NLR style
 */

/**
 * This interface makes use of a token, that is passed from one call
 * to another. The token is opaque and passed as the object of a class Object.
 * Conceptually, the token is a reference to a node from this tree.
 */
public interface IUnorderedBinaryTree extends IIterable, ICollection {

    /**
     * Appends the node with a provided value to the empty tree.
     * The node will become a root node of the tree.
     *
     * @param value To be added in the node to the tree.
     * @return A token that represents a root node.
     * @throws IllegalArgumentException if the tree is not empty
     */
    public Object append(double value);

    /**
     * Appends the node to the left of a node referenced by a token.
     *
     * @param node  The token that references the parent node
     * @param value To be added in the node to the tree.
     * @return A token that represents an added node.
     * @throws IllegalArgumentException if the node already has a left child
     *                                  or if the token does not reference a node from this tree
     */
    public Object appendLeft(Object node, double value);

    /**
     * Appends the node to the right of a node referenced by a token.
     *
     * @param node  The token that references the parent node
     * @param value To be added in the node to the tree.
     * @return A token that represents an added node.
     * @throws IllegalArgumentException if the node already has a right child
     *                                  or if the token does not reference a node from this tree
     */
    public Object appendRight(Object node, double value);
}


