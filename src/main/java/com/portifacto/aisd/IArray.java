package com.portifacto.aisd;

/**
 * Contract/guarantees:
 * 1. Structure: array
 * 2. Access: random, by index
 * 3. Value: double
 * 4. Uniqueness: duplicates possible
 * 5. Order: ordered by index
 * 6. Permanence: over-write is possible
 * 7. Iterator: none provided
 */

public interface IArray extends ICollection {

    /**
     * Sets the field at 'index' to 'value'. Any previous value is destroyed.
     *
     * @param index Index of the field, if the implementation does not
     *              say otherwise it is from zero to capacity-1
     * @param value Value to be inserted at 'index'
     * @throws ArrayIndexOutOfBoundsException if the index is out of range
     */
    public void set(int index, double value);

    /**
     * Returns the value of the field at 'index'; the value of the field remain unchanged
     *
     * @param index Index of the field, if the implementation does not
     *              say otherwise it is from zero to capacity-1
     * @return the value at index, if the value has never been set, the default value
     * is returned (implementation-specific)
     * @throws ArrayIndexOutOfBoundsException if the index is out of range
     */
    public double get(int index);

}
