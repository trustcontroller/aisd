package com.portifacto.aisd;

import java.util.ArrayDeque;
import java.util.Deque;

public class BinaryOrderedTree implements IOrderedTree {

    private int size = 0;

    private enum TreeTraversal {
        BREADTH_FIRST,
        DEPTH_FIRST_NLR,
        DEPTH_FIRST_LNR,
        DEPTH_FIRST_LRN,

    }

    static protected class Node {
        protected double value;
        protected Node left;
        protected Node right;
        protected int height;

        Node(double value) {
            this.value = value;
            this.left = null;
            this.right = null;
            this.height = 0;
        }
    }

    protected Node root = null;

    @Override
    public boolean add(double value) {
        Node node = new Node(value);
        if (root == null) {
            root = node;
            size++;
            return true;
        }
        return add(root, node);
    }

    private boolean add(Node current, Node node) {
        if (node.value == current.value) {
            return false;
        }
        if (current.left == null && node.value < current.value) {
            current.left = node;
            size++;
            return true;
        }
        if (current.right == null && node.value > current.value) {
            current.right = node;
            size++;
            return true;
        }
        if (node.value < current.value) {
            return add(current.left, node);
        }
        return add(current.right, node);
    }

    @Override
    public boolean find(double value) {
        if (root == null) return false;
        return find(root, value);
    }

    private boolean find(Node node, double value) {
        if (node == null) return false;
        if (node.value == value) return true;
        if (value < node.value) return find(node.left, value);
        else return (find(node.right, value));
    }

    @Override
    public boolean isEmpty() {
        return root == null;
    }

    @Override
    public int capacity() {
        return Integer.MAX_VALUE;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public IIterator lrnIterator() {
        return traverse(TreeTraversal.DEPTH_FIRST_LRN, root).iterator();
    }

    @Override
    public IIterator lnrIterator() {
        return traverse(TreeTraversal.DEPTH_FIRST_LNR, root).iterator();
    }

    @Override
    public IIterator iterator() {
        return traverse(TreeTraversal.DEPTH_FIRST_LNR, root).iterator();
    }

    @Override
    public IIterator nlrIterator() {
        return traverse(TreeTraversal.DEPTH_FIRST_NLR, root).iterator();
    }

    @Override
    public IIterator bfIterator() {
        return traverse(TreeTraversal.BREADTH_FIRST, root).iterator();
    }

    private IList traverse(TreeTraversal traversal, Node root) {
        IList result = new LinkedList();
        if (root == null) return result;
        if (traversal == TreeTraversal.BREADTH_FIRST) traverseBreadthFirst(root, result);
        else traverseDepthFirst(root, result, traversal);
        return result;
    }

    private void traverseDepthFirst(Node node, IList result, TreeTraversal traversal) {
        if (node == null) return;
        switch (traversal) {
            case DEPTH_FIRST_LNR: // in-order
                traverseDepthFirst(node.left, result, traversal);
                result.add(node.value);
                traverseDepthFirst(node.right, result, traversal);
                return;
            case DEPTH_FIRST_LRN: // post-order
                traverseDepthFirst(node.left, result, traversal);
                traverseDepthFirst(node.right, result, traversal);
                result.add(node.value);
                return;
            case DEPTH_FIRST_NLR: // pre-order
                result.add(node.value);
                traverseDepthFirst(node.left, result, traversal);
                traverseDepthFirst(node.right, result, traversal);
                return;
        }
    }

    private void traverseBreadthFirst(Node root, IList result) {
        Deque<Node> queue = new ArrayDeque<>();
        queue.add(root);
        while (!queue.isEmpty()) {
            Node node = queue.pollFirst();
            result.add(node.value);
            if (node.left != null) queue.add(node.left);
            if (node.right != null) queue.add(node.right);
        }
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        if (root == null) {
            sb.append("[x]");
        } else {
            sb.append(toString(root));
        }
        return sb.toString();
    }

    private String toString(Node node) {
        if (node == null) return "x";
        StringBuilder sb = new StringBuilder();
        sb.append('[');
        sb.append(node.value);
        sb.append(',');
        sb.append(toString(node.left));
        sb.append(',');
        sb.append(toString(node.right));
        sb.append(']');
        return sb.toString();
    }


}
