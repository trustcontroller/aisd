package com.portifacto.aisd;

import java.util.Arrays;

/**
 * Contract/guarantees: as in ISet
 */
public class HashSet implements ISet {

    protected final static int DEFAULT_CAPACITY = 11;
    protected final static int INCREMENTAL_CAPACITY = 11;

    private final static double MAX_LOAD_FACTOR = 0.75;

    private Double[] array;

    private int size; // number of stored elements

    public HashSet() {
        this(DEFAULT_CAPACITY);
    }

    public HashSet(int capacity) {
        array = new Double[capacity];
        Arrays.fill(array, null);
        size = 0;
    }

    @Override
    public boolean add(double value) {
        if (size >= array.length * MAX_LOAD_FACTOR) resize();
        boolean result = add(value, array);
        if (result) size++;
        return result;
    }

    private boolean add(double value, Double[] array) {
        int position = Math.abs(Double.valueOf(value).hashCode()) % array.length;
        if (array[position] == null) {
            array[position] = value;
            return true;
        }
        for (int i = 1; i < array.length; i++) {
            int potential = (position + i) % array.length;
            if (array[potential] == null) {
                array[potential] = value;
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean find(double value) {
        return find(value, array);
    }

    protected boolean find(double value, Double[] array) {
        int position = Math.abs(Double.valueOf(value).hashCode()) % array.length;
        for (int i = 0; i < array.length; i++) {
            int potential = (position + i) % array.length;
            if (array[potential] == null) return false;
            if (array[potential].equals(value)) return true;
        }
        return false;
    }

    private void resize() {
        Double[] larger = new Double[array.length + INCREMENTAL_CAPACITY];
        for (Double d : array) if (d != null) add(d, larger);
        array = larger;
    }

    protected IList getAsList() {
        IList result = new LinkedList();
        for (Double d : array) if (d != null) result.add(d);
        return result;
    }

    @Override
    public IIterator iterator() {

        return new HashSetIterator(array);
    }

    public static class HashSetIterator implements IIterator {

        private final Double[] array;
        private int current;

        public HashSetIterator(Double[] array) {
            this.array = array;
            this.current = 0;
        }

        @Override
        public Double get() {
            while (current < array.length) {
                if (array[current] != null) {
                    double result = array[current];
                    current++;
                    return result;
                }
                current++;
            }
            return null;
        }
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public int capacity() {
        return Integer.MAX_VALUE;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append('[');
        for (Double d : array) {
            if (d != null) {
                sb.append(d);
                sb.append(',');
            }
        }
        sb.append(']');
        return sb.toString();
    }

}
