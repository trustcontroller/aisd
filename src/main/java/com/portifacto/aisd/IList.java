package com.portifacto.aisd;

/**
 * Contract/guarantees:
 * 1. Structure: list
 * 3. Value: double
 * 4. Uniqueness: implementation-dependent
 * 4.a. unordered: duplicates possible
 * 4.b. ordered: no duplicates
 * 5. Order: implementation-dependent
 * 5.a. unordered: defined by the order of addition
 * (straight or inverse)
 * 5.b. ordered: defined by the order of values
 * (smallest to largest)
 * 6. Permanence: removal possible, contract assured
 * 7. Iterator: linear, from first to last
 */

public interface IList extends ICollection, IIterable {

    /**
     * Adds a value to the list; for unordered list the value is always added to the list
     * even if the identical value exists; for ordered list the value is added to the
     * list only if there is no identical value.
     *
     * @param value the value to be added to the list
     * @return true only if the value was added, false otherwise
     */
    public boolean add(double value);

    /**
     * Checks whether the list contains at least once a value
     *
     * @param value the value that is checked
     * @return true if the value is in the list, false otherwise
     */
    public boolean find(double value);

    /**
     * Removes a single occurrence of the value from the list, if such a value existed on the list;
     * if the list contains several identical values then any one of them is removed
     *
     * @param value the value to be removed
     * @return true if the value has been found and removed, false otherwise
     */
    public boolean remove(double value);
}
