package com.portifacto.aisd;

public class MinimumSpanningTree extends Helper {

    private double weight;
    private Edge[] tree = new Edge[0];

    private final IGraph graph;

    public MinimumSpanningTree(IGraph graph) {
        if (graph == null) throw new IllegalArgumentException();
        this.graph = graph;
    }

    public boolean minimumSpanningTree() {
        boolean[] included = new boolean[graph.getNodeCount()];
        double[] weights = new double[graph.getNodeCount()];
        int[] proposer = new int[graph.getNodeCount()];
        for (int i = 0; i < weights.length; i++) {
            weights[i] = Double.POSITIVE_INFINITY;
            proposer[i] = -1;
        }
        weights[0] = 0;

        while (findUnvisited(included) >= 0) {
            double minWeight = Double.POSITIVE_INFINITY;
            int candidate = -1;
            for (int i = 0; i < included.length; i++)
                if (!included[i] && weights[i] < minWeight) {
                    minWeight = weights[i];
                    candidate = i;
                }
            included[candidate] = true;
            if (proposer[candidate] >= 0) {
                Edge pair = new Edge(graph.getNodeName(proposer[candidate]), graph.getNodeName(candidate));
                Edge[] updated = new Edge[tree.length + 1];
                for (int i = 0; i < tree.length; i++) updated[i] = tree[i];
                updated[tree.length] = pair;
                tree = updated;
                if (graph.isEdgePresent(proposer[candidate], candidate) &&
                        graph.getEdgeWeight(proposer[candidate], candidate) != null)
                    weight += graph.getEdgeWeight(proposer[candidate], candidate);
            }
            for (int i = 0; i < included.length; i++) {
                if (!included[i] && graph.isEdgePresent(candidate, i)) {
                    if (graph.getEdgeWeight(candidate, i) != null && graph.getEdgeWeight(candidate, i) < weights[i]) {
                        weights[i] = graph.getEdgeWeight(candidate, i);
                        proposer[i] = candidate;
                    }
                }
            }
        }
        return true;
    }

    public double getWeight() {
        return weight;
    }

    public Edge[] getTree() {
        return tree;
    }


}
