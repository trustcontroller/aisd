package com.portifacto.aisd;

/**
 * Contract/guarantees: as in IStack
 */

public class ArrayStack extends ExpandableStorage implements IStack {

    private int stackPointer = 0;

    public ArrayStack() {
        super();
    }

    public ArrayStack(int capacity) {
        super(capacity);
    }

    @Override
    public boolean push(double value) {
        if (stackPointer >= storage.length) incrementCapacity();
        storage[stackPointer] = value;
        stackPointer++;
        return true;
    }

    @Override
    public Double pull() {
        if (stackPointer == 0) return null;
        stackPointer--;
        return storage[stackPointer];
    }
}
