package com.portifacto.aisd;

public class MergeSort extends Helper implements ISort {

    @Override
    public double[] sort(double[] data, int size) {
        if (data == null || size < 0 || size > data.length) throw new IllegalArgumentException();
        double[] local = makeCopy(data, size);
        arrayMergeSort(local, size);
        return local;
    }

    private void arrayMergeSort(double[] data, int size) {
        for (int length = 1; length < size; length *= 2) {
            double[] result = new double[size];
            int current = 0;

            for (int pos = 0; pos <= size / (length * 2); pos++) {
                int i = (pos * 2) * length;
                int j = (pos * 2 + 1) * length;
                int run1 = Integer.min(length, size - i);
                int run2 = Integer.min(length, size - j);

                while ((run1 > 0) || (run2 > 0)) {
                    if (run1 <= 0) {
                        result[current++] = data[j++];
                        run2--;
                    } else if (run2 <= 0) {
                        result[current++] = data[i++];
                        run1--;
                    } else {
                        if (data[i] >= data[j]) {
                            result[current++] = data[j++];
                            run2--;
                        } else {
                            result[current++] = data[i++];
                            run1--;
                        }
                    }
                }
            }
            for (int i = 0; i < size; i++) data[i] = result[i];
        }
    }
}
