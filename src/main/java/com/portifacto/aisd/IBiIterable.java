package com.portifacto.aisd;

/**
 * Contract/guarantees:
 * 1. All elements of the ADS will be delivered by the iterator
 * and by the reverse iterator
 * 2. Each complete iteration exhausts the iterator
 * 3. Subsequent iterators delivers elements in the same order
 * provided that the ADS did not change
 * 4. The order of reverse iterator is exactly reverse to the
 * order of the iterator
 * 5. The actual order of elements is implementation-specific
 * 6. No guarantees in case the ADS change during iteration
 * 7. No thread safety
 */
public interface IBiIterable extends IIterable {

    /**
     * Returns the reverse iterator for the collection, i.e. the iterator
     * that enumerates in exactly the revers order than the regular one
     *
     * @return The reverse iterator
     */
    public IIterator reverseIterator();

}
