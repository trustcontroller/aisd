package com.portifacto.aisd;

public class HierarchicalQueueList implements IHierarchicalQueue {

    private static final int DEFAULT_PRIORITY = 5;

    private static class ListElement {
        ListElement next;
        QueueElement head;
        QueueElement tail;
        int priority;

        ListElement(int priority) {
            this.priority = priority;
            next = null;
            head = null;
            tail = null;
        }
    }

    private static class QueueElement {
        QueueElement next;
        Double value;

        QueueElement(Double value) {
            this.value = value;
            this.next = null;
        }
    }

    ListElement head = null;
    int size = 0;

    @Override
    public boolean isEmpty() {
        return head == null;
    }

    @Override
    public int size() {
        return size;
    }

    public boolean offer(double value) {
        return offer(value, DEFAULT_PRIORITY);
    }

    public boolean offer(double value, int priority) {
        ListElement previous = null;
        ListElement selected = null;
        for (ListElement current = head; current != null; previous = current, current = current.next) {
            if (current.priority < priority) {
                selected = new ListElement(priority);
                if (previous == null) {
                    selected.next = head;
                    head = selected;
                } else {
                    previous.next = selected;
                    selected.next = current;
                }
                break;
            } else if (current.priority == priority) {
                selected = current;
                break;
            }
        }
        if (selected == null) {
            selected = new ListElement(priority);
            if (previous == null) head = selected;
            else previous.next = selected;
        }
        QueueElement element = new QueueElement(value);
        if (selected.head == null) selected.head = element;
        if (selected.tail != null) selected.tail.next = element;
        selected.tail = element;
        size++;
        return true;
    }

    public Double poll() {
        if (head == null || size == 0) return null;
        Double result = head.head.value;
        head.head = head.head.next;
        if (head.head == null) {
            head = head.next;
        }
        size--;
        return result;
    }

    public int capacity() {
        return Integer.MAX_VALUE;
    }

    public int used() {
        return size;
    }


}
