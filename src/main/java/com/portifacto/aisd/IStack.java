package com.portifacto.aisd;

/**
 * Contract/guarantees:
 * 1. Structure: stack
 * 3. Value: double
 * 4. Uniqueness: duplicates possible
 * 5. Order: strict LIFO (last-in first-out
 * 6. Permanence: removal is destructive
 * 7. Iterator: none
 */

public interface IStack {

    /**
     * Pushes a value to the top of the stack.
     *
     * @param value Value to be pushed, duplicates are allowed
     * @return true if pushed, false otherwise
     */
    public boolean push(double value);

    /**
     * Pulls a value form the top of the stack, if there is any
     *
     * @return The value, null if the stack if empty
     */
    public Double pull();

}

