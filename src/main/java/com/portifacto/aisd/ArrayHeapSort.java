package com.portifacto.aisd;

public class ArrayHeapSort extends Helper implements ISort {

    @Override
    public double[] sort(double[] data, int size) {
        if (data == null || size < 0 || size > data.length) throw new IllegalArgumentException();
        double[] local = makeCopy(data, size);
        arrayHeapSort(local, size);
        return local;
    }

    private void arrayHeapSort(double[] heap, int size) {
        // first, make a heap into a heap, i.e. bubble up the largest element
        //        for (int i = (heap.size()-1)/2; i >= 0; i--)
        for (int i = (size - 1); i >= 0; i--) {
            maxheap(heap, i, (size - 1));
        }

        // now move the highest at the end and re-do the heap
        for (int i = size - 1; i > 0; i--) {
            swap(heap, 0, i); // the highest went to the end
            maxheap(heap, 0, i - 1); // repair the heap from zero to just before swap inclusive

        }
    }


    private void maxheap(double[] heap, int first, int last) {

        int left = 2 * first + 1;
        int right = 2 * first + 2;
        int max = first;

        if (left <= last && heap[left] > heap[max]) max = left;
        if (right <= last && heap[right] > heap[max]) max = right;

        if (max != first) {
            swap(heap, first, max);
            maxheap(heap, max, last);
        }
    }

}
