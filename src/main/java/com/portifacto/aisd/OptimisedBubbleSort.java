package com.portifacto.aisd;

public class OptimisedBubbleSort extends Helper implements ISort {

    @Override
    public double[] sort(double[] data, int size) {
        if (data == null || size < 0 || size > data.length) throw new IllegalArgumentException();
        double[] local = makeCopy(data, size);
        optimisedBubbleSort(local, size);
        return local;
    }

    private void optimisedBubbleSort(double[] data, int size) {
        boolean swapped = true;
        int n = size;
        while (swapped) {
            swapped = false;
            for (int i = 0; i < n - 1; i++) {
                if (data[i] > data[i + 1]) {
                    swap(data, i, i + 1);
                    swapped = true;
                }
            }
            n--;
        }
    }

}
