package com.portifacto.aisd;

import java.util.UUID;

public class MyLittlePony implements Comparable<MyLittlePony> {

    static enum Colour {
        GREEN,
        BLUE,
        SILVER,
        GOLD,
        WHITE,
        PINK,
        RAINBOW
    }

    private final UUID uuid;

    private final Colour colour;

    public MyLittlePony(Colour colour) {
        this.colour = colour;
        this.uuid = UUID.randomUUID();
    }

    @Override
    public int compareTo(MyLittlePony another) {
        if (this.colour == another.colour) return 0;
        if (this.colour == Colour.PINK) return 1; // pink beats all
        // gold beats silver, both ways
        if (this.colour == Colour.GOLD && another.colour == Colour.SILVER) return 1;
        if (this.colour == Colour.SILVER && another.colour == Colour.GOLD) return -1;
        return this.colour.ordinal() < another.colour.ordinal() ? 1 : -1;
    }

    @Override
    public int hashCode() {
        return uuid.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof MyLittlePony)) return false;
        return uuid.equals(((MyLittlePony) o).uuid);
    }


}
