package com.portifacto.aisd;

public class ArrayGraph implements IGraph {

    private static class GraphEdge {

        private final Double weight;

        public GraphEdge(Double weight) {
            this.weight = weight;
        }

        public Double getWeight() {
            return weight;
        }

    }

    private static class GraphNode {

        private final String name;
        private final Double weight;

        public GraphNode(String name, Double weight) {
            this.name = name;
            this.weight = weight;
        }

        public String getName() {
            return name;
        }

        public Double getWeight() {
            return weight;
        }

    }

    private GraphNode[] graphNodes;
    private GraphEdge[][] graphEdges;

    public ArrayGraph() {
        this.graphNodes = new GraphNode[0];
        this.graphEdges = new GraphEdge[0][0];
    }

    @Override
    public void addNode(String name) {
        addNode(name, null);
    }

    @Override
    public void addNode(String name, double weight) {
        addNode(name, Double.valueOf(weight));
    }

    private void addNode(String name, Double weight) {
        if (name == null) throw new IllegalArgumentException();
        for (GraphNode node : graphNodes) {
            if (name.compareTo(node.getName()) == 0) {
                throw new IllegalArgumentException("Node name must be unique");
            }
        }

        GraphNode[] newGraphNodes = new GraphNode[graphNodes.length + 1];
        for (int i = 0; i < graphNodes.length; i++) newGraphNodes[i] = graphNodes[i];

        newGraphNodes[graphNodes.length] = new GraphNode(name, weight);

        GraphEdge[][] newGraphEdges = new GraphEdge[graphNodes.length + 1][graphNodes.length + 1];
        for (int i = 0; i < graphNodes.length; i++) {
            for (int j = 0; j < graphNodes.length; j++) {
                newGraphEdges[i][j] = graphEdges[i][j];
            }
        }

        graphNodes = newGraphNodes;
        graphEdges = newGraphEdges;
    }

    @Override
    public void addDirectedEdge(String from, String to, double weight) {
        addDirectedEdge(from, to, Double.valueOf(weight));
    }

    @Override
    public void addDirectedEdge(String from, String to) {
        addDirectedEdge(from, to, null);
    }

    private void addDirectedEdge(String from, String to, Double weight) {
        Integer indexFrom = getNodeIndex(from);
        Integer indexTo = getNodeIndex(to);
        if (indexFrom == null || indexTo == null)
            throw new IllegalArgumentException();

        if (graphEdges[indexFrom][indexTo] != null)
            throw new IllegalArgumentException("Edge already defined");

        graphEdges[indexFrom][indexTo] = new GraphEdge(weight);

    }

    @Override
    public void addUndirectedEdge(String from, String to, double weight) {
        addUndirectedEdge(from, to, Double.valueOf(weight));
    }

    @Override
    public void addUndirectedEdge(String from, String to) {
        addUndirectedEdge(from, to, null);
    }

    private void addUndirectedEdge(String from, String to, Double weight) {
        Integer indexFrom = getNodeIndex(from);
        Integer indexTo = getNodeIndex(to);
        if (indexFrom == null || indexTo == null)
            throw new IllegalArgumentException();

        if (graphEdges[indexFrom][indexTo] != null)
            throw new IllegalArgumentException("Edge already defined");

        if (graphEdges[indexTo][indexFrom] != null)
            throw new IllegalArgumentException("Edge already defined");

        graphEdges[indexFrom][indexTo] = new GraphEdge(weight);
        graphEdges[indexTo][indexFrom] = new GraphEdge(weight);
    }

    @Override
    public void addBiDirectedEdge(String from, String to, double fromTo, double toFrom) {
        addDirectedEdge(from, to, fromTo);
        addDirectedEdge(to, from, toFrom);
    }

    @Override
    public int getNodeCount() {
        return graphNodes.length;
    }

    @Override
    public Integer getNodeIndex(String name) {
        if (name == null)
            throw new IllegalArgumentException();
        for (int i = 0; i < graphNodes.length; i++) {
            if (name.compareTo(graphNodes[i].getName()) == 0) return i;
        }
        return null;
    }

    @Override
    public Double getNodeWeight(int index) {
        if (index < 0 || index >= graphNodes.length) return null;
        return graphNodes[index].getWeight();

    }

    @Override
    public String getNodeName(int index) {
        if (index < 0 || index >= graphNodes.length) return null;
        return graphNodes[index].getName();

    }

    @Override
    public boolean isEdgePresent(int from, int to) {
        if (from < 0 || from >= graphNodes.length
                || to < 0 || to >= graphNodes.length) return false;
        return graphEdges[from][to] != null;

    }

    @Override
    public Double getEdgeWeight(int from, int to) {
        if (from < 0 || from >= graphNodes.length
                || to < 0 || to >= graphNodes.length) return null;
        return graphEdges[from][to] == null ? null : graphEdges[from][to].getWeight();
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < graphNodes.length; i++) {
            sb.append(graphNodes[i].getName());
            if (graphNodes[i].getWeight() != null) {
                sb.append('(');
                sb.append(graphNodes[i].getWeight());
                sb.append(')');
            }
            sb.append(" -> ");
            for (int j = 0; j < graphNodes.length; j++) {
                if (graphEdges[i][j] != null) {
                    sb.append(graphNodes[j].getName());
                    if (graphEdges[i][j].getWeight() != null) {
                        sb.append('(');
                        sb.append(graphEdges[i][j].getWeight());
                        sb.append(')');
                    }
                    sb.append(' ');
                }
            }
            sb.append('\n');
        }
        return sb.toString();
    }


}
