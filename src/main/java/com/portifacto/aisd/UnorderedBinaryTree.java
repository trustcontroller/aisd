package com.portifacto.aisd;

public class UnorderedBinaryTree implements IUnorderedBinaryTree {

    public static class Node {
        final UnorderedBinaryTree tree;
        final double value;
        Node left;
        Node right;

        public Node(UnorderedBinaryTree tree, double value) {
            this.tree = tree;
            this.value = value;
            this.left = null;
            this.right = null;
        }

    }

    public UnorderedBinaryTree() {

    }

    private Node root = null;

    private int size = 0;

    @Override
    public Object append(double value) {
        if (root != null)
            throw new IllegalArgumentException();
        Node alter = new Node(this, value);
        root = alter;
        size++;
        return alter;
    }

    @Override
    public Object appendLeft(Object node, double value) {
        if (!(node instanceof Node) || ((Node) node).tree != this || ((Node) node).left != null)
            throw new IllegalArgumentException();
        Node alter = new Node(this, value);
        ((Node) node).left = alter;
        size++;
        return alter;
    }

    @Override
    public Object appendRight(Object node, double value) {
        if (!(node instanceof Node) || ((Node) node).tree != this || ((Node) node).right != null)
            throw new IllegalArgumentException();
        Node alter = new Node(this, value);
        ((Node) node).right = alter;
        size++;
        return alter;
    }

    @Override
    public int capacity() {
        return Integer.MAX_VALUE;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return root == null;
    }

    @Override
    public IIterator iterator() {
        IList list = new LinkedList();
        visitNode(list, root);
        return list.iterator();
    }

    private void visitNode(IList list, Node node) {
        if (node == null) return;
        list.add(node.value);
        visitNode(list, node.left);
        visitNode(list, node.right);
    }
}
