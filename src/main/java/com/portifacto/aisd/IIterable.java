package com.portifacto.aisd;

/**
 * Contract/guarantees:
 * 1. All elements of the ADS will be delivered by the iterator
 * 2. Each complete iteration exhausts the iterator
 * 3. Subsequent iterators delivers elements in the same order
 * provided that the ADS did not change
 * 4. The order of elements is implementation-specific
 * 5. No guarantees in case the ADS change during iteration
 * 6. No thread safety
 */

public interface IIterable {
    /**
     * Returns an iterator object for the collection; each iterator enumerates all elements
     * from the collection, but the order of enumeration depends on the implementation.
     * Once the iterator reaches the end of the collection, it must not be reused.
     * The collection must not be modified while the iterator is in use.
     *
     * @return an iterator
     */
    public IIterator iterator();

}
