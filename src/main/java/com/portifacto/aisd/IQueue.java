package com.portifacto.aisd;

/**
 * Contract/guarantees:
 * 1. Structure: queue
 * 3. Value: double
 * 4. Uniqueness: duplicates possible
 * 5. Order: implementation-specific
 * if not stated, then order of insertion
 * 6. Persistence:
 * 6.a. destructive removal
 * b.b. if size bound, the insertion not always successful
 * 7. Iterator: none
 */

public interface IQueue extends ICollection {

    /**
     * Inserts the value at the end of the queue. Duplicate values are allowed
     *
     * @param value The value to be inserted
     * @return true if inserted, fail otherwise
     */
    public boolean offer(double value);

    /**
     * Attempts to get the value at the head of the queue. If the queue is not empty, the
     * first value is removed from the queue and returned.
     *
     * @return the value at the head of the queue, null if the queue is empty
     */
    public Double poll();
}
