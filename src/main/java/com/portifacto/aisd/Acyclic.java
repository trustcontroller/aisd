package com.portifacto.aisd;

public class Acyclic extends Helper {

    private final IGraph graph;

    public Acyclic(IGraph graph) {
        if (graph == null) throw new IllegalArgumentException();
        this.graph = graph;
    }

    public boolean isAcyclic() {
        boolean[] visited = new boolean[graph.getNodeCount()];
        boolean[] path = new boolean[graph.getNodeCount()];

        int index;
        while ((index = findUnvisited(visited)) >= 0)
            if (hasBackEdge(graph, index, visited, path)) return false;

        return true;
    }


}
