package com.portifacto.aisd;

public interface IPonyList {

    public void add(MyLittlePony value);

    public boolean find(MyLittlePony value);

    public void remove(MyLittlePony value);

    public IPonyIterator iterator();

}
