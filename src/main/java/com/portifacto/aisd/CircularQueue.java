package com.portifacto.aisd;

/**
 * Contract/guarantees: as in IQueue. Clarifications:
 * 5. Order: order of insertion
 * 6. Permanence:
 * 6.a. destructive removal
 * 6.b. the insertion not always successful if the queue is full
 * 6.c. the size is fixed
 * 7. Iterator: none
 */

public class CircularQueue implements IQueue {

    protected static final int DEFAULT_SIZE = 10;

    private final double[] array;

    private int head = -1;
    private int tail = -1;

    public CircularQueue() {
        this(DEFAULT_SIZE);
    }

    public CircularQueue(int size) {
        array = new double[size];
    }

    @Override
    public boolean offer(double value) {
        if ((head == 0 && tail == array.length - 1) ||
                (tail == (head - 1) % (array.length - 1))) {
            return false;
        }
        if (head == -1) {
            head = 0;
            tail = 0;
            array[tail] = value;
        } else if (tail == array.length - 1 && head != 0) {
            tail = 0;
            array[tail] = value;
        } else {
            tail++;
            array[tail] = value;

        }
        return true;
    }

    @Override
    public Double poll() {
        if (head == -1) {
            return null;
        }
        Double data = array[head];
        if (head == tail) {
            head = -1;
            tail = -1;
        } else if (head == array.length - 1) {
            head = 0;
        } else {
            head++;
        }
        return data;

    }

    @Override
    public boolean isEmpty() {
        return head == -1;
    }

    @Override
    public int capacity() {
        return array.length;
    }

    @Override
    public int size() {
        if (head == -1) return 0;
        return (array.length + tail + 1 - head) % array.length;
    }

}
