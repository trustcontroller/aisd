package com.portifacto.aisd;

public class Colouring {

    private final IGraph graph;

    private int[] result = new int[0];

    public Colouring(IGraph graph) {
        if (graph == null) throw new IllegalArgumentException();
        this.graph = graph;
    }

    public boolean colouring(int numColours) {
        int[] colours = new int[graph.getNodeCount()];

        int carry = 0;
        while (carry == 0) {
            // verify if this is acceptable
            boolean success = true;
            for (int i = 0; i < graph.getNodeCount() && success; i++) {
                for (int j = 0; j < graph.getNodeCount() && success; j++) {
                    if (i != j && graph.isEdgePresent(i, j) && colours[i] == colours[j]) {
                        success = false;
                    }
                }
            }

            // save the output and exit if acceptable
            if (success) {
                result = colours;
                return true;
            }

            // not acceptable, determine the next set of colours
            carry = 1;
            for (int i = 0; i < colours.length; i++) {
                colours[i] += carry;
                carry = 0;
                if (colours[i] >= numColours) {
                    colours[i] = 0;
                    carry = 1;
                }
            }

        }
        return false;
    }

    public int[] getResult() {
        return result;
    }

}
