package com.portifacto.aisd;

public class FibonacciArray implements IArray {

    private static final int MAX_FIBONACCI = 70;

    @Override
    public void set(int index, double value) {
        if (index < 0 || index > MAX_FIBONACCI) throw new ArrayIndexOutOfBoundsException(index);
    }

    @Override
    public double get(int index) {
        if (index < 0 || index > MAX_FIBONACCI) throw new ArrayIndexOutOfBoundsException(index);
        if (index == 0) return 0;
        if (index == 1) return 1;
        long previous = 0;
        long current = 1;
        for (int i = 2; i <= index; i++) {
            final long next = previous + current;
            previous = current;
            current = next;
        }
        return current;
    }

    @Override
    public int capacity() {
        return MAX_FIBONACCI;
    }

    @Override
    public int size() {
        return MAX_FIBONACCI;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

}

