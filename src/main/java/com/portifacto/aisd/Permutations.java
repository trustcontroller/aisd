package com.portifacto.aisd;

public class Permutations {

    private int[] initialList;


    IPermutationCallback callback;

    public Permutations(int n, IPermutationCallback callback) {
        initialList = new int[n];
        for (int i = 0; i < n; i++) initialList[i] = i;
        this.callback = callback;
    }

    public Permutations(int[] initialList, IPermutationCallback callback) {
        this.initialList = initialList;
        this.callback = callback;
    }

    public void generate() {
        int[] permutation = new int[initialList.length];
        for (int i = 0; i < initialList.length; i++) permutation[i] = initialList[i];
        generate(initialList.length, permutation);
    }


    private void generate(int k, int[] permutation) {
        if (k == 1) {
            callback.process(permutation);
            return;
        }
        generate(k - 1, permutation);

        for (int i = 0; i < k - 1; i++) {
            if (k % 2 == 0) swap(permutation, i, k - 1);
            else swap(permutation, 0, k - 1);
            generate(k - 1, permutation);
        }
    }

    private void swap(int[] permutation, int x, int y) {
        int a = permutation[x];
        permutation[x] = permutation[y];
        permutation[y] = a;
    }
}
