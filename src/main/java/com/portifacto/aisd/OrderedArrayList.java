package com.portifacto.aisd;

public class OrderedArrayList extends ExpandableStorage implements IList, ISet {

    private int size = 0;

    @Override
    public boolean add(double value) {
        if (size >= storage.length) incrementCapacity();
        int insertAt;
        for (insertAt = 0; insertAt < size; insertAt++) {
            if (value == storage[insertAt]) return false;
            if (value < storage[insertAt]) {
                break;
            }
        }
        for (int j = size - 1; j >= insertAt; j--) {
            storage[j + 1] = storage[j];
        }
        storage[insertAt] = value;
        size++;
        return true;
    }

    @Override
    public boolean find(double value) {
        int left = 0;
        int right = size - 1;
        while (left <= right) {
            int midway = (left + right) / 2;
            if (value == storage[midway]) return true;
            if (value < storage[midway]) right = midway - 1;
            else left = midway + 1;
        }
        return false;
    }

    @Override
    public boolean remove(double value) {
        int left = 0;
        int right = size - 1;
        while (left <= right) {
            int midway = (left + right) / 2;
            if (value == storage[midway]) {
                for (int j = midway; j < size - 1; j++)
                    storage[j] = storage[j + 1];
                size--;
                return true;
            }
            if (value < storage[midway]) right = midway - 1;
            else left = midway + 1;
        }
        return false;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public int capacity() {
        return super.capacity();
    }

    @Override
    public IIterator iterator() {
        return new ArrayIterator(storage, size);
    }

    static class ArrayIterator implements IIterator {

        private final double[] storage;
        private final int size;
        private int index;

        ArrayIterator(double[] storage, int size) {
            this.storage = storage;
            this.size = size;
            this.index = 0;
        }

        public Double get() {
            if (size == 0) return null;
            if (index >= size) return null;
            return storage[index++];
        }
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public String toString() {
        return listPrettyPrint(this.iterator());
    }


}
