package com.portifacto.aisd;

/**
 * Conventions:
 * null as data: not possible
 * duplicate values: allowed, stored separately, find or delete acts on any copy
 * order preservation: strict
 */
public class BiLinkedList extends Helper implements IList, IBiIterable {

    private static class ListElement {
        ListElement next;
        ListElement previous;
        double value;

        ListElement(double value) {
            this.value = value;
            this.next = null;
            this.previous = null;
        }
    }

    private ListElement head = null;
    private ListElement tail = null;

    private int size = 0;

    @Override
    public boolean add(double value) {
        ListElement element = new ListElement(value);
        if (head == null) {
            head = element;
            tail = element;
            size++;
            return true;
        }
        tail.next = element;
        element.previous = tail;
        tail = element;
        size++;
        return true;
    }

    @Override
    public boolean find(double value) {
        ListElement current = head;
        while (current != null) {
            if (current.value == value) return true;
            current = current.next;
        }
        return false;
    }

    @Override
    public boolean remove(double value) {
        ListElement current = head;
        while (current != null) {
            if (current.value == value) {
                if (current.previous == null) head = current.next;
                else current.previous.next = current.next;
                if (current.next == null) tail = current.previous;
                else current.next.previous = current.previous;
                size--;
                return true;
            }
            current = current.next;
        }
        return false;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public int capacity() {
        return Integer.MAX_VALUE;
    }

    @Override
    public IIterator iterator() {
        return new ListIterator(head);
    }

    static class ListIterator implements IIterator {

        ListElement current;

        ListIterator(ListElement head) {
            current = head;
        }

        public Double get() {
            if (current == null) return null;
            double result = current.value;
            current = current.next;
            return result;
        }
    }

    @Override
    public IIterator reverseIterator() {
        return new ReverseListIterator(tail);
    }

    static class ReverseListIterator implements IIterator {

        ListElement current;

        ReverseListIterator(ListElement tail) {
            current = tail;
        }

        public Double get() {
            if (current == null) return null;
            double result = current.value;
            current = current.previous;
            return result;
        }
    }

    @Override
    public boolean isEmpty() {
        return head == null;
    }

    @Override
    public String toString() {
        return listPrettyPrint(this.iterator());
    }

}
