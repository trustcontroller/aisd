package com.portifacto.aisd;

public interface ISort {

    public double[] sort(double[] data, int size);

    default double[] sort(double[] data) {
        return sort(data, data.length);
    }

}
