package com.portifacto.aisd;

public class ShortestPath {

    private final IGraph graph;

    private double calculatedDistance = Double.POSITIVE_INFINITY;
    private String[] calculatedPath = new String[0];

    public ShortestPath(IGraph graph) {
        if (graph == null) throw new IllegalArgumentException();
        this.graph = graph;
    }

    public boolean shortestPath(String from, String to) {
        if (from == null || to == null) throw new IllegalArgumentException();
        if (graph.getNodeCount() == 0) throw new IllegalArgumentException();
        // convert names into indexes
        int indexFrom = graph.getNodeIndex(from);
        int indexTo = graph.getNodeIndex(to);

        if (indexFrom < 0 || indexTo < 0) throw new IllegalArgumentException();

        // initialize variables
        boolean[] visited = new boolean[graph.getNodeCount()];
        double[] distance = new double[graph.getNodeCount()];
        int[] previous = new int[graph.getNodeCount()];

        for (int i = 0; i < graph.getNodeCount(); i++) {
            distance[i] = Double.POSITIVE_INFINITY;
            previous[i] = -1;
        }
        distance[indexFrom] = 0;
        visited[indexFrom] = true;

        int newCurrent = indexFrom;
        int current;

        // the loop should run for as long as there are new nodes
        while (newCurrent >= 0) {

            // this one becomes the current node
            current = newCurrent;
            visited[current] = true;

            // visit neighbours, for unvisited update the distance
            for (int i = 0; i < graph.getNodeCount(); i++) {
                if (graph.isEdgePresent(current, i) &&
                        graph.getEdgeWeight(current, i) != null
                        && !visited[i]) {
                    double proposed = distance[current] + graph.getEdgeWeight(current, i);
                    if (proposed < distance[i]) {
                        distance[i] = proposed;
                        previous[i] = current;
                    }
                }
            }

            // find unvisited with the smallest distance
            double minDistance = Double.POSITIVE_INFINITY;
            newCurrent = -1;
            for (int i = 0; i < graph.getNodeCount(); i++) {
                if (!visited[i] && distance[i] < minDistance) {
                    newCurrent = i;
                    minDistance = distance[i];
                }
            }

            // if the one found is the target, stop
            if (newCurrent == indexTo) {
                calculatedDistance = distance[newCurrent];
                // find the actual path by travelling back, and save it
                int count = 0;
                for (int index = indexTo; index >= 0; index = previous[index]) {
                    count++;
                }
                calculatedPath = new String[count];
                int pos = count - 1;
                for (int index = indexTo; index >= 0; index = previous[index]) {
                    calculatedPath[pos--] = graph.getNodeName(index);
                }
                return true;
            }
        }
        return false;
    }

    public double getDistance() {
        return calculatedDistance;
    }

    public String[] getPath() {
        return calculatedPath;
    }


}
