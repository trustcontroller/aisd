package com.portifacto.aisd;

import java.util.Optional;

public interface IPonyIterator {
    public Optional<MyLittlePony> get();
}
