package com.portifacto.aisd;

public class QuickSort extends Helper implements ISort {

    @Override
    public double[] sort(double[] data, int size) {
        if (data == null || size < 0 || size > data.length)
            throw new IllegalArgumentException();
        double[] local = makeCopy(data, size);
        quickSort(local, 0, size - 1);
        return local;
    }

    private void quickSort(double[] data, int start, int stop) {
        if (start >= stop) return;
        double pivot = data[stop];

        int i = start;
        for (int j = start; j < stop; j++) {
            if (data[j] <= pivot) {
                swap(data, i, j);
                i++;
            }
        }

        swap(data, stop, i);

        quickSort(data, start, i - 1);
        quickSort(data, i + 1, stop);
    }

}
