package com.portifacto.aisd;

public class Edge {
    public String x;
    public String y;

    public Edge(String x, String y) {
        this.x = x;
        this.y = y;
    }
}
