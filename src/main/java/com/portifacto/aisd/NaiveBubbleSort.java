package com.portifacto.aisd;

/**
 * Conventions:
 * null or malformed input: not allowed
 */

public class NaiveBubbleSort extends Helper implements ISort {

    @Override
    public double[] sort(double[] data, int size) {
        if (data == null || size < 0 || size > data.length)
            throw new IllegalArgumentException();
        double[] local = makeCopy(data, size);
        naiveBubbleSort(local, size);
        return local;
    }

    private void naiveBubbleSort(double[] data, int size) {
        for (int i = 0; i < size - 1; i++) {
            for (int j = 0; j < size - i - 1; j++) {
                if (data[j] > data[j + 1])
                    swap(data, j, j + 1);
            }
        }
    }

}
