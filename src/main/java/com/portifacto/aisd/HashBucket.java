package com.portifacto.aisd;

/**
 * Contract/guarantees: as in ISet
 */
public class HashBucket implements ISet {

    private final static int TABLE_SIZE = 11;

    final private IList[] array;
    private int size = 0;

    public HashBucket(int size) {
        array = new IList[size];
        for (int i = 0; i < array.length; i++) {
            array[i] = new OrderedLinkedList();
        }
    }

    public HashBucket() {
        this(TABLE_SIZE);
    }

    @Override
    public boolean add(double value) {
        int position = Math.abs(Double.valueOf(value).hashCode()) % array.length;
        boolean result = array[position].add(value);
        if (result) size++;
        return result;
    }

    @Override
    public boolean find(double value) {
        int position = Math.abs(Double.valueOf(value).hashCode()) % array.length;
        return array[position].find(value);
    }

    @Override
    public IIterator iterator() {
        return new HashBucketIterator(array);
    }

    public static class HashBucketIterator implements IIterator {

        private final IList[] array;
        private int position;
        private IIterator iterator;

        public HashBucketIterator(IList[] array) {
            this.array = array;
            this.position = 0;
            iterator = position < array.length ? array[position].iterator() : null;
        }

        @Override
        public Double get() {
            if (iterator == null) return null;
            while (position < array.length) {
                Double result = iterator.get();
                if (result != null) return result;
                position++;
                iterator = position < array.length ? array[position].iterator() : null;
            }
            return null;
        }

    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public int capacity() {
        return Integer.MAX_VALUE;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }


}
