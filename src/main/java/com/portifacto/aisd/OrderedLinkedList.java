package com.portifacto.aisd;

public class OrderedLinkedList extends NaiveLinkedList implements IList, ISet {

    public enum Order {
        MIN_TO_MAX,
        MAX_TO_MIN
    }

    private final Order order;
    private int size = 0;

    public OrderedLinkedList(Order order) {
        this.order = order;
    }

    public OrderedLinkedList() {
        this(Order.MIN_TO_MAX);
    }

    @Override
    public boolean add(double value) {
        ListElement element = new ListElement(value);
        ListElement previous = null;
        for (ListElement current = head; current != null; current = current.next) {
            if (current.value == value) return false;
            if (((current.value > value) && (order == Order.MIN_TO_MAX)) ||
                    ((current.value < value) && (order == Order.MAX_TO_MIN))) {
                if (previous == null) {
                    element.next = head;
                    head = element;
                    size++;
                    return true;
                }
                previous.next = element;
                element.next = current;
                size++;
                return true;
            }
            previous = current;
        }
        if (previous == null) head = element;
        else previous.next = element;
        element.next = null;
        size++;
        return true;
    }

    @Override
    public boolean find(double value) {
        for (ListElement current = head; current != null; current = current.next) {
            if (current.value == value) return true;
            if (current.value > value) return false;
        }
        return false;
    }

    @Override
    public boolean remove(double value) {
        for (ListElement current = head, previous = null; current != null;
             current = current.next) {
            if (current.value == value) {
                if (previous == null) head = current.next;
                else previous.next = current.next;
                size--;
                return true;
            }
            if (current.value > value) return false;
            previous = current;
        }
        return false;
    }

    @Override
    public int capacity() {
        return Integer.MAX_VALUE;
    }

    @Override
    public int size() {
        return size;
    }

}
