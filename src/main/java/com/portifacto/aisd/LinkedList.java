package com.portifacto.aisd;

/**
 * Conventions:
 * null as data: not possible
 * duplicate values: allowed, stored separately, find or delete acts on any copy
 * order preservation: strict, in order of addition
 */

public class LinkedList extends NaiveLinkedList implements IList {

    ListElement tail = null;

    @Override
    public boolean add(double value) {
        ListElement current = new ListElement(value);
        if (head == null) head = current;
        if (tail != null) tail.next = current;
        tail = current;
        return true;
    }

    private Object monitor = new Object();

    public void threadSafeAdd(double value) {
        ListElement current = new ListElement(value);
        synchronized (monitor) {
            if (head == null) {
                head = current;
                tail = current;
                return;
            }
        }
        synchronized (monitor) {
            tail.next = current;
            tail = current;
        }
    }

    public boolean findRecursive(double value) {
        return findRecursive(head, value);
    }

    private boolean findRecursive(ListElement element, double value) {
        if (element == null) return false;
        if (element.value == value) return true;
        return findRecursive(element.next, value);
    }

}
