package com.portifacto.aisd;

public class BalancedBinaryOrderedTree extends BinaryOrderedTree implements IOrderedTree {

    // this is the AVL tree (Adelson-Velsky and Landis)

    @Override
    public boolean add(double value) {
        Node node = new Node(value);
        Node result = add(root, node);
        if (result == null) return false;
        root = result;
        return true;
    }

    private Node add(Node current, Node node) {
        if (current == null) {
            return node;
        }
        if (current.value == node.value) {
            return null;
        }
        if (node.value < current.value) {
            Node result = add(current.left, node);
            if (result == null) return null;
            current.left = result;
        } else {
            Node result = add(current.right, node);
            if (result == null) return null;
            current.right = result;
        }
        return reBalance(current);
    }

    Node reBalance(Node node) {
        updateHeight(node);
        int balance = getBalance(node);
        if (balance > 1) {
            if (height(node.right.right) > height(node.right.left)) {
                node = rotateLeft(node);
            } else {
                node.right = rotateRight(node.right);
                node = rotateLeft(node);
            }
        } else if (balance < -1) {
            if (height(node.left.left) > height(node.left.right))
                node = rotateRight(node);
            else {
                node.left = rotateLeft(node.left);
                node = rotateRight(node);
            }
        }
        return node;
    }

    private void updateHeight(Node node) {
        node.height = 1 + Math.max(height(node.left),
                height(node.right));
    }

    private int height(Node node) {
        return node == null ? -1 : node.height;
    }

    private int getBalance(Node node) {
        return (node == null) ? 0 :
                height(node.right) -
                        height(node.left);
    }

    Node rotateLeft(Node node) {
        Node x = node.right;
        Node z = x.left;
        x.left = node;
        node.right = z;
        updateHeight(node);
        updateHeight(x);
        return x;
    }

    Node rotateRight(Node node) {
        Node x = node.left;
        Node z = x.right;
        x.right = node;
        node.left = z;
        updateHeight(node);
        updateHeight(x);
        return x;
    }

    protected int height() {
        if (root == null) return 0;
        return root.height;
    }

}
