package com.portifacto.aisd;

/**
 * Contract/guarantees:
 * 1. Multi-threading: safety not guaranteed
 * 2. Concurrent modifications: detection not guaranteed
 * 3. Multiple usage: no, single usage
 * 4. Termination: null, repeatedly
 * 5. Order: as defined by the implementation
 */

public interface IIterator {

    /**
     * Returns the next value from the collection
     *
     * @return the value, or null if there is no more values to be enumerated
     */
    public Double get();
}
