package com.portifacto.aisd;

public class SparseArray implements IArray {

    private final static int CAPACITY_INCREMENT = 10;

    private final static double DEFAULT_VALUE = 0.0;

    private static class Element {
        int index;
        double value;

        Element(int index, Double value) {
            this.index = index;
            this.value = value;
        }
    }

    private Element[] storage;

    private int effective = 0; // number of elements kept
    private int maximum = -1; // maximum index stored

    public SparseArray(int capacity) {
        if (capacity < 0) throw new IllegalArgumentException();
        storage = new Element[capacity];
    }

    public SparseArray() {
        this(CAPACITY_INCREMENT);
    }

    @Override
    public double get(int index) {
        if (index < 0)
            throw new ArrayIndexOutOfBoundsException(Integer.toString(index));
        if (index > maximum) return DEFAULT_VALUE;
        for (int i = 0; i < effective; i++)
            if (storage[i].index == index) return storage[i].value;
        return DEFAULT_VALUE;
    }

    @Override
    public void set(int index, double value) {
        if (index < 0)
            throw new ArrayIndexOutOfBoundsException(Integer.toString(index));
        if (index > maximum) {
            addElement(index, value);
            return;
        }
        for (int i = 0; i < effective; i++)
            if (storage[i].index == index) {
                storage[i].value = value;
                return;
            }
        addElement(index, value);
    }

    private void addElement(int index, double value) {
        if (effective >= storage.length) {
            Element[] newStorage = new Element[storage.length + CAPACITY_INCREMENT];
            for (int i = 0; i < storage.length; i++) newStorage[i] = storage[i];
            storage = newStorage;
        }
        storage[effective++] = new Element(index, value);
        maximum = Math.max(maximum, index);
    }

    @Override
    public int capacity() {
        return Integer.MAX_VALUE;
    }

    @Override
    public int size() {
        return maximum;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }


}



