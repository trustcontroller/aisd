package com.portifacto.aisd;

public class TravellingSalesmanProblem implements IPermutationCallback {

    double minLength = Double.POSITIVE_INFINITY;
    int[] minPath;
    private final IGraph graph;
    private int originator = -1;

    Permutations permutations;

    public TravellingSalesmanProblem(IGraph graph, String start) {
        if (graph == null || start == null) throw new IllegalArgumentException();
        this.graph = graph;
        if (graph.getNodeCount() == 0) return;
        int[] list = new int[graph.getNodeCount() - 1];
        minPath = new int[graph.getNodeCount() - 1];
        int pos = 0;
        for (int i = 0; i < graph.getNodeCount(); i++) {
            if (graph.getNodeName(i).compareTo(start) == 0) {
                originator = i;
            } else {
                list[pos++] = i;
            }
        }
        if (originator < 0) throw new IllegalArgumentException();
        permutations = new Permutations(list, this);
    }

    @Override
    public void process(int[] permutation) {
        double currentLength = 0;
        // calculate length including starting node, infinity if none
        int predecessor = originator;
        for (int successor : permutation) {
            if (!graph.isEdgePresent(predecessor, successor) || graph.getEdgeWeight(predecessor, successor) == null) {
                currentLength = Double.POSITIVE_INFINITY;
                break;
            }
            currentLength += graph.getEdgeWeight(predecessor, successor);
            predecessor = successor;
        }
        // add the last leg: predecessor back to the starting node
        if (graph.isEdgePresent(predecessor, originator) && graph.getEdgeWeight(predecessor, originator) != null) {
            currentLength += graph.getEdgeWeight(predecessor, originator);
        } else {
            currentLength = Double.POSITIVE_INFINITY;
        }
        // update and store current path if shorter
        if (currentLength < minLength) {
            minLength = currentLength;
            for (int i = 0; i < minPath.length; i++) minPath[i] = permutation[i];
        }
    }

    public double calculate() {
        permutations.generate();
        return minLength;
    }

    public String[] getMinPath() {
        String[] result = new String[minPath.length + 2];
        int position = 0;
        result[position++] = graph.getNodeName(originator);
        for (int j : minPath) result[position++] = graph.getNodeName(j);
        result[position++] = graph.getNodeName(originator);
        return result;
    }

}
