package com.portifacto.aisd;

/**
 * Contract/guarantees: as in IArray
 */

public class ExpandableArray extends ExpandableStorage implements IArray {

    double DEFAULT_VALUE = 0.0;

    private int used = 0;

    public ExpandableArray(int capacity) {
        super(capacity);
    }

    public ExpandableArray() {
        super();
    }

    @Override
    public double get(int index) {
        if (index < 0)
            throw new ArrayIndexOutOfBoundsException(Integer.toString(index));
        if (index >= storage.length)
            return DEFAULT_VALUE;
        return storage[index];
    }

    @Override
    public void set(int index, double value) {
        if (index < 0)
            throw new ArrayIndexOutOfBoundsException(Integer.toString(index));
        if (index >= storage.length) incrementCapacity(index);
        storage[index] = value;
        used = Math.max(index, used);
    }

    @Override
    public int capacity() {
        return Integer.MAX_VALUE;
    }

    @Override
    public int size() {
        return used;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }


}

