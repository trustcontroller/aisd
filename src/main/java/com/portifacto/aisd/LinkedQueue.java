package com.portifacto.aisd;

public class LinkedQueue implements IQueue {

    private static class QueueElement {
        QueueElement next;
        double value;

        QueueElement(double value) {
            this.value = value;
            this.next = null;
        }
    }

    private int capacity;
    private int length = 0;

    QueueElement head = null;
    QueueElement tail = null;

    public LinkedQueue(int capacity) {
        this.capacity = capacity;
    }

    public LinkedQueue() {
        this(Integer.MAX_VALUE);
    }

    @Override
    public boolean offer(double value) {
        if (length >= capacity) return false;
        QueueElement element = new QueueElement(value);
        if (head == null) head = element;
        if (tail != null) tail.next = element;
        tail = element;
        length++;
        return true;
    }

    @Override
    public Double poll() {
        if (length == 0 || head == null) return null;
        QueueElement element = head;
        head = element.next;
        return element.value;
    }

    @Override
    public int capacity() {
        return capacity;
    }

    @Override
    public int size() {
        return length;
    }

    @Override
    public boolean isEmpty() {
        return length == 0;
    }


}
