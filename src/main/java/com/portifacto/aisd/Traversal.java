package com.portifacto.aisd;

public class Traversal extends Helper {

    private final IGraph graph;

    private final boolean[] visited;
    private final String[] result;
    private int position;

    public Traversal(IGraph graph) {
        if (graph == null) throw new IllegalArgumentException();
        this.graph = graph;
        visited = new boolean[graph.getNodeCount()];
        result = new String[graph.getNodeCount()];
    }

    public String[] traversal() {
        int index;
        while ((index = findUnvisited(visited)) >= 0)
            traversal(index);
        return result;
    }

    private int traversal(int index) {
        if (visited[index]) return position;
        visited[index] = true;
        result[position++] = graph.getNodeName(index);
        for (int i = 0; i < graph.getNodeCount(); i++) {
            if (graph.isEdgePresent(index, i)) {
                position = traversal(i);
            }
        }
        return position;
    }

}
