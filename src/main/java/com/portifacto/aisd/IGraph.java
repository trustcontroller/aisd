package com.portifacto.aisd;

public interface IGraph {

    /**
     * Adds the node to the graph. The order is not guaranteed
     * (i.e. the first added may not have an index==0 etc.)
     * The node does not have any weight defined.
     * Note that the weight is not the identifier.
     *
     * @param name unique name of the node, not null
     * @throws IllegalArgumentException for non-unique or null
     */
    public void addNode(String name);

    /**
     * Adds the node to the graph with the specific weight.
     * The order of addition is not guaranteed
     * (i.e. the first added may not have an index==0 etc.)
     * Note that the weight is not the identifier.
     *
     * @param name    unique name of the node, not null
     * @param weight, the weight of the node
     * @throws IllegalArgumentException for non-unique or null
     */
    public void addNode(String name, double weight);

    /**
     * Adds the directed edge from one named node to another named node;
     * both names can be identical; the edge must not already exist
     *
     * @param from   name of the node from where the edge starts
     * @param to     name of the node from where the edge starts
     * @param weight the weight associated with the node
     * @throws IllegalArgumentException if names are null or not found or already exists
     */
    public void addDirectedEdge(String from, String to, double weight);

    /**
     * Adds the directed edge from one named node to another named node;
     * both names can be identical; the edge must not already exist
     *
     * @param from name of the node from where the edge starts
     * @param to   name of the node from where the edge starts
     * @throws IllegalArgumentException if names are null or not found or already exists
     */
    public void addDirectedEdge(String from, String to);

    /**
     * Adds two directed edges between one named node and another named node,
     * where each directed edge may have a different weight;
     * both names can be identical; the edge (or any part of it) must not already exist
     *
     * @param from name of the node from where the edge starts
     * @param to   name of the node from where the edge starts
     * @throws IllegalArgumentException if names are null or not found or already exists
     */
    public void addBiDirectedEdge(String from, String to, double fromTo, double toFrom);

    /**
     * Adds the undirected edge between one named node and another named node;
     * both names can be identical; the edge or any part of it (i.e. in one direction)
     * must not already exist.
     *
     * @param from   name of the node from where the edge starts
     * @param to     name of the node from where the edge starts
     * @param weight the weight associated with the node
     * @throws IllegalArgumentException if names are null or not found or already exists
     */
    public void addUndirectedEdge(String from, String to, double weight);

    /**
     * Adds the undirected edge between one named node and another named node;
     * both names can be identical; the edge or any part of it (i.e. in one direction)
     * must not already exist.
     *
     * @param from name of the node from where the edge starts
     * @param to   name of the node from where the edge starts
     * @throws IllegalArgumentException if names are null or not found or already exists
     */
    public void addUndirectedEdge(String from, String to);

    /**
     * Reports the number of nodes in the graph; nodes have their indexes
     * always from the range 0...count-1. No specific order of nodes is
     * assured
     *
     * @return The number of node sin the graph; zero for empty graph
     */
    public int getNodeCount();

    /**
     * Returns the index of a node of a given name
     *
     * @param name as provided at the creation of the node
     * @return index, 0...count-1, null if no node of this name
     */
    public Integer getNodeIndex(String name);

    /**
     * Returns the weight associated with a node of a given index
     *
     * @param index of the node, 0...count-1
     * @return weight, if defined, null if not defined or it
     * the index is outside its range
     */
    public Double getNodeWeight(int index);

    /**
     * Returns the name of the node, i.e. the string provided at the
     * creation of the node
     *
     * @param index index of the node, from 0 to count-1
     * @return name of the node, null if index is incorrect
     */
    public String getNodeName(int index);

    /**
     * Reports whether there is an edge (weighted or not) from and to
     * specified nodes, identified by their indexes
     *
     * @param from index of the starting node of the edge, 0...count-1
     * @param to   index of the terminating node of the edge, 0...count-1
     * @return true if there is an edge, weighted or not, false otherwise
     */
    public boolean isEdgePresent(int from, int to);

    /**
     * Reports the weight of the specific edge
     *
     * @param from index of the starting node of the edge, 0...count-1
     * @param to   index of the terminating node of the edge, 0...count-1
     * @return the value of the weight, null if there is no weight
     * or if there is no edge
     */
    public Double getEdgeWeight(int from, int to);

}
