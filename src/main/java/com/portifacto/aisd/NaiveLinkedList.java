package com.portifacto.aisd;

/**
 * Conventions:
 * null as data: not possible
 * duplicate values: allowed, stored separately, find or delete acts on any copy
 * order preservation: strictly inverse
 */
public class NaiveLinkedList extends Helper implements IList {

    protected ListElement head = null;
    private int size = 0;

    protected static class ListElement {
        ListElement next;
        double value;

        ListElement(double value) {
            this.value = value;
            this.next = null;
        }
    }

    @Override
    public boolean add(double value) {
        ListElement current = new ListElement(value);
        current.next = head;
        head = current;
        size++;
        return true;
    }

    @Override
    public boolean find(double value) {
        ListElement current = head;
        while (current != null) {
            if (current.value == value) return true;
            current = current.next;
        }
        return false;
    }

    @Override
    public boolean remove(double value) {
        ListElement current = head;
        ListElement previous = null;
        while (current != null) {
            if (current.value == value) {
                if (previous == null) head = current.next;
                else previous.next = current.next;
                size--;
                return true;
            }
            previous = current;
            current = current.next;
        }
        return false;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public int capacity() {
        return Integer.MAX_VALUE;
    }

    @Override
    public IIterator iterator() {
        return new ListIterator(head);
    }

    static class ListIterator implements IIterator {

        ListElement current;

        ListIterator(ListElement head) {
            current = head;
        }

        public Double get() {
            if (current == null) return null;
            double result = current.value;
            current = current.next;
            return result;
        }
    }

    @Override
    public boolean isEmpty() {
        return head == null;
    }

    @Override
    public String toString() {
        return listPrettyPrint(this);
    }

}
