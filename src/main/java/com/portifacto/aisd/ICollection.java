package com.portifacto.aisd;

public interface ICollection {

    /**
     * verifies whether the collection is empty, i.e., contains no elements
     *
     * @return true if empty, false otherwise
     */
    public boolean isEmpty();

    /**
     * Reports the maximum capacity of the collection, i.e. the maximum number of elements
     * that the collection can hold
     *
     * @return maximum capacity; note that Integer.MAX_VALUE indicates the capacity
     * constrained only by available memory
     */
    public int capacity();

    /**
     * Reports the current number of elements in the collection; empty collection
     * has a size zero
     *
     * @return current number of elements
     */
    public int size();

}
