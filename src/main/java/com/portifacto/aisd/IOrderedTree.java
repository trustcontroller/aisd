package com.portifacto.aisd;

/**
 * Contract/guarantees:
 * 1. Tree structure: root and nodes, no cycles
 * 2. Number of children: limited by implementation
 * 3. Value: double
 * 4. Uniqueness: unique only
 * 5. Order: as defined by the value, by default from the smallest to the largest
 * 6. Permanence: only addition, no removal
 * 7. Iterator: depth-first, LNR; other as i traversable
 */
public interface IOrderedTree extends ISet, ITraversable {
}


