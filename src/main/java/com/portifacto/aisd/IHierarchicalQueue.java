package com.portifacto.aisd;

/**
 * Contract/guarantees:
 * -- as IQueue
 * 3. Value: double, with priority (higher priority == higher number)
 */

public interface IHierarchicalQueue extends IQueue {

    /**
     * Inserts a value into a hierarchical queue, at the end of a queue of
     * a priority. Duplicated values are allowed.
     *
     * @param value    Value to be inserted
     * @param priority Priority, the higher the value the higher the priority.
     * @return true if the insertion has been successful; false otherwise
     */
    public boolean offer(double value, int priority);

}
