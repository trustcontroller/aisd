package com.portifacto.aisd;

import org.testng.Assert;
import org.testng.annotations.Test;

public class ArrayHeapSortTest {

    @Test
    public void evenNumber() {
        double[] vector = {4.0, 3.0, 2.0, 6.0};
        double[] result = new ArrayHeapSort().sort(vector, vector.length);
        double[] expected = new OptimisedBubbleSort().sort(vector, vector.length);
        Assert.assertEquals(result, expected);
    }

    @Test
    public void oddNumber() {
        double[] vector = {4.0, 3.0, 2.0, 6.0, 1.0};
        double[] result = new ArrayHeapSort().sort(vector, vector.length);
        double[] expected = new OptimisedBubbleSort().sort(vector, vector.length);
        Assert.assertEquals(result, expected);
    }

}