package com.portifacto.aisd;

import org.testng.Assert;
import org.testng.annotations.Test;

public class TopologicalOrderTest {

    @Test
    public void emptyGraph() {
        IGraph graph = new ArrayGraph();

        TopologicalOrder topologicalOrder = new TopologicalOrder(graph);
        String[] result = topologicalOrder.topologicalOrder();

        Assert.assertNotNull(result);
        Assert.assertEquals(result.length, 0);
    }

    @Test
    public void twoNodesOnly() {
        IGraph graph = new ArrayGraph();
        graph.addNode("Gdansk");
        graph.addNode("Gdynia");
        graph.addDirectedEdge("Gdansk", "Gdynia");

        TopologicalOrder topologicalOrder = new TopologicalOrder(graph);
        String[] result = topologicalOrder.topologicalOrder();

        Assert.assertNotNull(result);
        Assert.assertEquals(result.length, 2);
        Assert.assertEquals(result[0], "Gdansk");
        Assert.assertEquals(result[1], "Gdynia");
    }

    @Test
    public void cyclicGraph() {
        IGraph graph = new ArrayGraph();
        graph.addNode("Gdansk");
        graph.addNode("Gdynia");
        graph.addDirectedEdge("Gdansk", "Gdynia");
        graph.addDirectedEdge("Gdynia", "Gdansk");

        TopologicalOrder topologicalOrder = new TopologicalOrder(graph);
        String[] result = topologicalOrder.topologicalOrder();

        Assert.assertNull(result);
    }

    @Test
    public void fiveNodes() {
        IGraph graph = new ArrayGraph();
        graph.addNode("A");
        graph.addNode("B");
        graph.addNode("C");
        graph.addNode("D");
        graph.addNode("E");

        graph.addDirectedEdge("B", "A");
        graph.addDirectedEdge("B", "E");
        graph.addDirectedEdge("B", "D");
        graph.addDirectedEdge("C", "A");
        graph.addDirectedEdge("D", "C");
        graph.addDirectedEdge("E", "D");
        graph.addDirectedEdge("E", "A");

        TopologicalOrder topologicalOrder = new TopologicalOrder(graph);
        String[] result = topologicalOrder.topologicalOrder();

        Assert.assertNotNull(result);
        Assert.assertEquals(result.length, 5);
        Assert.assertEquals(result[0], "B");
        Assert.assertEquals(result[1], "E");
        Assert.assertEquals(result[2], "D");
        Assert.assertEquals(result[3], "C");
        Assert.assertEquals(result[4], "A");
    }

}