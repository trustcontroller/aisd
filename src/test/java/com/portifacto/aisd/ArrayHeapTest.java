package com.portifacto.aisd;

import org.testng.Assert;
import org.testng.annotations.Test;

public class ArrayHeapTest extends TestHelper {

    @Test
    public void emptyHeapEmpty() {
        ArrayHeap heap = new ArrayHeap();
        Assert.assertTrue(heap.isEmpty());
    }

    @Test
    public void emptyHeapNoIteration() {
        ArrayHeap heap = new ArrayHeap();
        IIterator iterator = heap.iterator();
        Double result = iterator.get();
        Assert.assertNull(result);
    }

    @Test
    public void definedCapacityEmpty() {
        ArrayHeap heap = new ArrayHeap(10, ArrayHeap.Order.MAX_HEAP);
        IIterator iterator = heap.iterator();
        Double result = iterator.get();
        Assert.assertNull(result);
    }

    @Test
    public void oneElementMinHeap() {
        ArrayHeap heap = new ArrayHeap(10, ArrayHeap.Order.MIN_HEAP);
        heap.add(6.0);
        Assert.assertTrue(identical(heap.iterator(), new double[]{6.0}));
    }

    @Test
    public void oneElementDefaultHeap() {
        ArrayHeap heap = new ArrayHeap();
        heap.add(6.0);
        Assert.assertTrue(identical(heap.iterator(), new double[]{6.0}));
    }

    @Test
    public void evenElements() {
        ArrayHeap heap = new ArrayHeap();
        heap.add(6.0);
        heap.add(4.0);
        heap.add(2.0);
        heap.add(3.0);
        heap.add(8.0);
        heap.add(5.0);
        Assert.assertTrue(identical(heap.iterator(), new double[]{8.0, 6.0, 5.0, 3.0, 4.0, 2.0}));
    }

    @Test
    public void oddElements() {
        ArrayHeap heap = new ArrayHeap();
        heap.add(6.0);
        heap.add(4.0);
        heap.add(2.0);
        heap.add(3.0);
        heap.add(8.0);
        Assert.assertTrue(identical(heap.iterator(), new double[]{8.0, 6.0, 2.0, 3.0, 4.0}));
    }

    @Test
    public void findExisting() {
        ArrayHeap heap = new ArrayHeap();
        heap.add(6.0);
        heap.add(4.0);
        Assert.assertTrue(heap.find(6.0));
    }

    @Test
    public void findNonExisting() {
        ArrayHeap heap = new ArrayHeap();
        heap.add(6.0);
        heap.add(4.0);
        Assert.assertFalse(heap.find(8.0));
    }

    @Test
    public void sizing() {
        ArrayHeap heap = new ArrayHeap();
        heap.add(6.0);
        heap.add(4.0);
        Assert.assertEquals(heap.size(), 2);
    }

    @Test
    public void duplicatesAllowed() {
        ArrayHeap heap = new ArrayHeap();
        heap.add(6.0);
        heap.add(6.0);
        Assert.assertEquals(heap.size(), 2);
    }

    @Test
    public void capacityUnbound() {
        ArrayHeap heap = new ArrayHeap();
        Assert.assertEquals(heap.capacity(), Integer.MAX_VALUE);
    }

    @Test
    public void offerTwo() {
        ArrayHeap heap = new ArrayHeap();
        Assert.assertTrue(heap.offer(6.0));
        Assert.assertTrue(heap.offer(4.0));
        Assert.assertEquals(heap.size(), 2);
    }

    @Test
    public void offerAndPoll() {
        ArrayHeap heap = new ArrayHeap();
        Assert.assertTrue(heap.offer(4.0));
        Assert.assertTrue(heap.offer(8.0));
        Assert.assertTrue(heap.offer(6.0));
        Assert.assertEquals(heap.size(), 3);
        Assert.assertEquals(heap.poll().doubleValue(), 8.0);
        Assert.assertEquals(heap.poll().doubleValue(), 6.0);
        Assert.assertEquals(heap.poll().doubleValue(), 4.0);
        Assert.assertNull(heap.poll());
        Assert.assertTrue(heap.isEmpty());
    }

    @Test
    public void offerAndPollMinHeap() {
        ArrayHeap heap = new ArrayHeap(10, ArrayHeap.Order.MIN_HEAP);
        Assert.assertTrue(heap.offer(4.0));
        Assert.assertTrue(heap.offer(8.0));
        Assert.assertTrue(heap.offer(6.0));
        Assert.assertEquals(heap.size(), 3);
        Assert.assertEquals(heap.poll().doubleValue(), 4.0);
        Assert.assertEquals(heap.poll().doubleValue(), 6.0);
        Assert.assertEquals(heap.poll().doubleValue(), 8.0);
        Assert.assertNull(heap.poll());
        Assert.assertTrue(heap.isEmpty());
    }

    @Test
    public void evenNumbers() {
        double[] vector = {4.0, 3.0, 2.0, 6.0};
        double[] result = new ArrayHeap().sort(vector, vector.length);
        Assert.assertTrue(identical(result, new double[]{2.0, 3.0, 4.0, 6.0}));
    }

    @Test
    public void oddNumbers() {
        double[] vector = {4.0, 3.0, 2.0, 6.0, 1.0};
        double[] result = new ArrayHeap().sort(vector, vector.length);
        Assert.assertTrue(identical(result, new double[]{1.0, 2.0, 3.0, 4.0, 6.0}));
    }

    @Test
    public void totalInversion() {
        double[] vector = {4.0, 3.0, 2.0, 1.0};
        double[] result = new ArrayHeap().sort(vector, vector.length);
        Assert.assertTrue(identical(result, new double[]{1.0, 2.0, 3.0, 4.0}));
    }

    @Test
    public void duplicateValues() {
        double[] vector = {4.0, 3.0, 4.0, 3.0};
        double[] result = new ArrayHeap().sort(vector, vector.length);
        Assert.assertTrue(identical(result, new double[]{3.0, 3.0, 4.0, 4.0}));
    }

    @Test
    public void orderPreserved() {
        double[] vector = {1.0, 2.0, 3.0, 4.0};
        double[] result = new ArrayHeap().sort(vector, vector.length);
        Assert.assertTrue(identical(result, new double[]{1.0, 2.0, 3.0, 4.0}));
    }

    @Test
    public void reverseSort() {
        double[] vector = {1.0, 2.0, 3.0, 4.0};
        double[] result = new ArrayHeap(ArrayHeap.Order.MIN_HEAP).sort(vector, vector.length);
        Assert.assertTrue(identical(result, new double[]{4.0, 3.0, 2.0, 1.0}));
    }

    @Test
    public void zeroLength() {
        double[] vector = {};
        double[] result = new ArrayHeap().sort(vector, vector.length);
        Assert.assertTrue(identical(result, new double[]{}));
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void nullInput() {
        new ArrayHeap().sort(null, 0);
        Assert.fail();
    }

    @Test
    public void plainIterator() {
        ArrayHeap heap = new ArrayHeap();
        heap.add(23.0);
        heap.add(22.0);
        heap.add(21.0);
        heap.add(20.0);
        Assert.assertTrue(identical(heap.iterator(), new double[]{23.0, 22.0, 21.0, 20.0}));
    }

    @Test
    public void breadthFirstIterator() {
        ArrayHeap heap = new ArrayHeap();
        heap.add(23.0);
        heap.add(22.0);
        heap.add(21.0);
        heap.add(20.0);
        Assert.assertTrue(identical(heap.bfIterator(), new double[]{23.0, 22.0, 21.0, 20.0}));
    }

    @Test
    public void lnrIterator() {
        ArrayHeap heap = new ArrayHeap();
        heap.add(23.0);
        heap.add(22.0);
        heap.add(21.0);
        heap.add(20.0);
        Assert.assertTrue(identical(heap.lnrIterator(), new double[]{20.0, 22.0, 23.0, 21.0}));
    }

    @Test
    public void nlrIterator() {
        ArrayHeap heap = new ArrayHeap();
        heap.add(23.0);
        heap.add(22.0);
        heap.add(21.0);
        heap.add(20.0);
        Assert.assertTrue(identical(heap.nlrIterator(), new double[]{23.0, 22.0, 20.0, 21.0}));
    }

    @Test
    public void lrnIterator() {
        ArrayHeap heap = new ArrayHeap();
        heap.add(23.0);
        heap.add(22.0);
        heap.add(21.0);
        heap.add(20.0);
        Assert.assertTrue(identical(heap.lrnIterator(), new double[]{20.0, 22.0, 21.0, 23.0}));
    }

}