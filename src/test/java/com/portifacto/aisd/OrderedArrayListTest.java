package com.portifacto.aisd;

import org.testng.Assert;
import org.testng.annotations.Test;

public class OrderedArrayListTest extends TestHelper {

    @Test
    public void empty() {
        OrderedArrayList list = new OrderedArrayList();
        Assert.assertTrue(list.isEmpty());
        Assert.assertEquals(list.size(), 0);
        Assert.assertEquals(list.capacity(), Integer.MAX_VALUE);
    }

    @Test
    public void findEvenNumberOfElements() {
        OrderedArrayList a = new OrderedArrayList();
        a.add(10.0);
        a.add(20.0);
        a.add(30.0);
        a.add(40.0);
        a.add(50.0);
        a.add(60.0);

        Assert.assertTrue(identical(a, new double[]{10.0, 20.0, 30.0, 40.0, 50.0, 60.0}));

        Assert.assertTrue(a.find(10.0));
        Assert.assertTrue(a.find(20.0));
        Assert.assertTrue(a.find(30.0));
        Assert.assertTrue(a.find(40.0));
        Assert.assertTrue(a.find(50.0));
        Assert.assertTrue(a.find(60.0));
        Assert.assertFalse(a.find(0.0));
        Assert.assertFalse(a.find(70.0));
        Assert.assertFalse(a.find(25.0));
        Assert.assertFalse(a.find(35.0));
    }

    @Test
    public void findOddNumberOfElements() {
        OrderedArrayList a = new OrderedArrayList();
        a.add(10.0);
        a.add(20.0);
        a.add(30.0);
        a.add(40.0);
        a.add(50.0);

        Assert.assertTrue(identical(a, new double[]{10.0, 20.0, 30.0, 40.0, 50.0}));

        Assert.assertTrue(a.find(10.0));
        Assert.assertTrue(a.find(20.0));
        Assert.assertTrue(a.find(30.0));
        Assert.assertTrue(a.find(40.0));
        Assert.assertTrue(a.find(50.0));
        Assert.assertFalse(a.find(0.0));
        Assert.assertFalse(a.find(70.0));
        Assert.assertFalse(a.find(25.0));
        Assert.assertFalse(a.find(35.0));
    }

    @Test
    public void preserveOrder() {
        OrderedArrayList a = new OrderedArrayList();
        a.add(20.0);
        a.add(10.0);
        a.add(30.0);
        a.add(50.0);
        a.add(40.0);
        Assert.assertTrue(identical(a, new double[]{10.0, 20.0, 30.0, 40.0, 50.0}));
    }

    @Test
    public void removeFirst() {
        OrderedArrayList a = new OrderedArrayList();
        a.add(20.0);
        a.add(10.0);
        a.add(30.0);
        a.remove(10.0);
        Assert.assertTrue(identical(a, new double[]{20.0, 30.0}));
    }

    @Test
    public void removeLast() {
        OrderedArrayList a = new OrderedArrayList();
        a.add(20.0);
        a.add(10.0);
        a.add(30.0);
        a.remove(30.0);
        Assert.assertTrue(identical(a, new double[]{10.0, 20.0}));
    }

    @Test
    public void removeMiddle() {
        OrderedArrayList a = new OrderedArrayList();
        a.add(20.0);
        a.add(10.0);
        a.add(30.0);
        a.remove(20.0);
        Assert.assertTrue(identical(a, new double[]{10.0, 30.0}));
    }

    @Test
    public void string() {
        IList list = new OrderedArrayList();
        list.add(77.0);
        list.add(88.0);
        String s = list.toString();
        Assert.assertTrue(s.contains("77"));
        Assert.assertTrue(s.contains("88"));
    }

}