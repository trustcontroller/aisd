package com.portifacto.aisd;

import org.testng.Assert;
import org.testng.annotations.Test;

public class UnorderedArrayListTest extends TestHelper {

    @Test
    public void empty() {
        UnorderedArrayList a = new UnorderedArrayList();
        Assert.assertTrue(a.isEmpty());
    }

    @Test
    public void intialSize() {
        UnorderedArrayList a = new UnorderedArrayList(10);
        Assert.assertTrue(a.isEmpty());
        Assert.assertEquals(a.size(), 0);
        Assert.assertEquals(a.capacity(), Integer.MAX_VALUE);
    }

    @Test
    public void oneElement() {
        UnorderedArrayList a = new UnorderedArrayList();
        a.add(10.0);
        Assert.assertFalse(a.isEmpty());
        Assert.assertTrue(identical(a, new double[]{10.0}));
    }

    @Test
    public void findElement() {
        UnorderedArrayList a = new UnorderedArrayList();
        a.add(10.0);
        a.add(20.0);
        a.add(30.0);
        Assert.assertTrue(a.find(10.0));
        Assert.assertTrue(a.find(20.0));
        Assert.assertTrue(a.find(30.0));
    }

    @Test
    public void notFindElement() {
        UnorderedArrayList a = new UnorderedArrayList();
        a.add(10.0);
        a.add(20.0);
        a.add(30.0);
        Assert.assertFalse(a.find(0.0));
        Assert.assertFalse(a.find(15.0));
        Assert.assertFalse(a.find(40.0));
    }

    @Test
    public void removeFirst() {
        UnorderedArrayList a = new UnorderedArrayList();
        a.add(10.0);
        a.add(20.0);
        a.add(30.0);
        a.remove(10.0);
        Assert.assertTrue(identical(a, new double[]{20.0, 30.0}));
    }

    @Test
    public void removeLast() {
        UnorderedArrayList a = new UnorderedArrayList();
        a.add(10.0);
        a.add(20.0);
        a.add(30.0);
        a.remove(20.0);
        Assert.assertTrue(identical(a, new double[]{10.0, 30.0}));
    }

    @Test
    public void aboveCapacity() {
        UnorderedArrayList a = new UnorderedArrayList();
        a.add(1.0);
        a.add(2.0);
        a.add(3.0);
        a.add(4.0);
        a.add(5.0);
        a.add(6.0);
        a.add(7.0);
        a.add(8.0);
        a.add(9.0);
        a.add(10.0);
        a.add(11.0);
        a.add(12.0);
        Assert.assertTrue(identical(a,
                new double[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12}));
    }

    @Test
    public void removeMiddle() {
        UnorderedArrayList a = new UnorderedArrayList();
        a.add(10.0);
        a.add(20.0);
        a.add(30.0);
        a.remove(20.0);
        Assert.assertTrue(identical(a, new double[]{10.0, 30.0}));
    }

    @Test
    public void removeAll() {
        UnorderedArrayList a = new UnorderedArrayList();
        a.add(10.0);
        a.add(20.0);
        a.add(30.0);
        a.remove(20.0);
        a.remove(10.0);
        a.remove(30.0);
        Assert.assertTrue(a.isEmpty());
    }

    @Test
    public void string() {
        IList list = new UnorderedArrayList();
        list.add(77.0);
        list.add(88.0);
        String s = list.toString();
        Assert.assertTrue(s.contains("77"));
        Assert.assertTrue(s.contains("88"));
    }

}