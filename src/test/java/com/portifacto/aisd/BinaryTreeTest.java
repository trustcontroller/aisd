package com.portifacto.aisd;

import org.testng.Assert;
import org.testng.annotations.Test;

public class BinaryTreeTest extends TestHelper {

    @Test
    public void emptyTree() {
        BinaryOrderedTree tree = new BinaryOrderedTree();
        Assert.assertTrue(tree.isEmpty());
        Assert.assertEquals(tree.size(), 0);
        Assert.assertEquals(tree.capacity(), Integer.MAX_VALUE);
        Assert.assertNull(tree.iterator().get());
    }

    @Test
    public void oneElement() {
        BinaryOrderedTree tree = new BinaryOrderedTree();
        tree.add(6.0);
        Assert.assertEquals(tree.size(), 1);
        Assert.assertTrue(identical(tree.iterator(), new double[]{6.0}));
    }

    @Test
    public void theSameElement() {
        BinaryOrderedTree tree = new BinaryOrderedTree();
        tree.add(6.0);
        tree.add(6.0);
        Assert.assertTrue(identical(tree.iterator(),
                new double[]{6.0}));
    }

    @Test
    public void leftAddToRoot() {
        BinaryOrderedTree tree = new BinaryOrderedTree();
        tree.add(6.0);
        tree.add(4.0);
        Assert.assertTrue(identical(tree.iterator(),
                new double[]{4.0, 6.0}));
    }

    @Test
    public void rightAddToRoot() {
        BinaryOrderedTree tree = new BinaryOrderedTree();
        tree.add(6.0);
        tree.add(8.0);
        Assert.assertTrue(identical(tree.iterator(),
                new double[]{6.0, 8.0}));
    }

    @Test
    public void cascadingAddLeftLeft() {
        BinaryOrderedTree tree = new BinaryOrderedTree();
        tree.add(6.0);
        tree.add(4.0);
        tree.add(2.0);
        Assert.assertTrue(identical(tree.iterator(),
                new double[]{2.0, 4.0, 6.0}));
    }

    @Test
    public void cascadingAddLeftRight() {
        BinaryOrderedTree tree = new BinaryOrderedTree();
        tree.add(6.0);
        tree.add(4.0);
        tree.add(5.0);
        Assert.assertTrue(identical(tree.iterator(),
                new double[]{4.0, 5.0, 6.0}));
    }

    @Test
    public void cascadingAddRightLeft() {
        BinaryOrderedTree tree = new BinaryOrderedTree();
        tree.add(6.0);
        tree.add(8.0);
        tree.add(7.0);
        Assert.assertTrue(identical(tree.iterator(),
                new double[]{6.0, 7.0, 8.0}));
    }

    @Test
    public void cascadingAddRightRight() {
        BinaryOrderedTree tree = new BinaryOrderedTree();
        tree.add(6.0);
        tree.add(8.0);
        tree.add(9.0);
        Assert.assertTrue(identical(tree.iterator(),
                new double[]{6.0, 8.0, 9.0}));
    }

    @Test
    public void findExisting() {
        BinaryOrderedTree tree = new BinaryOrderedTree();
        tree.add(6.0);
        tree.add(4.0);
        tree.add(8.0);
        tree.add(5.0);
        tree.add(7.0);
        Assert.assertTrue(tree.find(6.0));
        Assert.assertTrue(tree.find(4.0));
        Assert.assertTrue(tree.find(8.0));
        Assert.assertTrue(tree.find(5.0));
        Assert.assertTrue(tree.find(7.0));
    }

    @Test
    public void findNotExisting() {
        BinaryOrderedTree tree = new BinaryOrderedTree();
        tree.add(6.0);
        tree.add(4.0);
        tree.add(8.0);
        tree.add(5.0);
        tree.add(7.0);
        Assert.assertFalse(tree.find(0.0));
        Assert.assertFalse(tree.find(4.5));
        Assert.assertFalse(tree.find(11.0));
        Assert.assertFalse(tree.find(7.25));
    }

    @Test
    public void defaultTraversal() {
        IOrderedTree tree = buildTree();
        Assert.assertTrue(identical(tree.iterator(),
                new double[]{2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0}));
    }

    @Test
    public void lnrTraversal() {
        IOrderedTree tree = buildTree();
        Assert.assertTrue(identical(tree.lnrIterator(),
                new double[]{2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0}));
    }

    @Test
    public void lrnTraversal() {
        BinaryOrderedTree tree = buildTree();
        Assert.assertTrue(identical(tree.lrnIterator(),
                new double[]{2.0, 4.0, 5.0, 3.0, 8.0, 7.0, 9.0, 6.0}));
    }

    @Test
    public void nlrTraversal() {
        BinaryOrderedTree tree = buildTree();
        Assert.assertTrue(identical(tree.nlrIterator(),
                new double[]{6.0, 3.0, 2.0, 5.0, 4.0, 9.0, 7.0, 8.0}));
    }

    @Test
    public void bfTraversal() {
        BinaryOrderedTree tree = buildTree();
        Assert.assertTrue(identical(tree.bfIterator(),
                new double[]{6.0, 3.0, 9.0, 2.0, 5.0, 7.0, 4.0, 8.0}));
    }

    private BinaryOrderedTree buildTree() {
        BinaryOrderedTree tree = new BinaryOrderedTree();
        tree.add(6.0);
        tree.add(3.0);
        tree.add(9.0);
        tree.add(2.0);
        tree.add(5.0);
        tree.add(4.0);
        tree.add(7.0);
        tree.add(8.0);
        return tree;
    }

    @Test
    public void printEmpty() {
        IOrderedTree tree = new BinaryOrderedTree();
        String s = tree.toString();
        Assert.assertTrue(s.contains("x"));
    }

    @Test
    public void printSome() {
        IOrderedTree tree = new BinaryOrderedTree();
        tree.add(88.0);
        tree.add(77.0);
        tree.add(99.0);
        String s = tree.toString();
        Assert.assertTrue(s.contains("77"));
        Assert.assertTrue(s.contains("88"));
        Assert.assertTrue(s.contains("99"));
    }

}