package com.portifacto.aisd;

import org.testng.Assert;
import org.testng.annotations.Test;

public class NaiveArrayTest {

    private static final double EPSILON = 0.0001;

    @Test
    public void nominal() {
        IArray array = new NaiveArray();
        array.set(8, 21.0);
        Assert.assertEquals(array.get(8), 21.0, EPSILON);
    }

    @Test
    public void zeroIndex() {
        IArray array = new NaiveArray(5);
        array.set(0, 21.0);
        Assert.assertEquals(array.get(0), 21.0, EPSILON);
    }

    @Test
    public void maximumIndex() {
        IArray array = new NaiveArray(5);
        array.set(4, 21.0);
        Assert.assertEquals(array.get(4), 21.0, EPSILON);
    }

    @Test(expectedExceptions = ArrayIndexOutOfBoundsException.class)
    public void negativeSetIndex() {
        IArray array = new NaiveArray(5);
        array.set(-1, 21.0);
        Assert.fail();
    }

    @Test(expectedExceptions = ArrayIndexOutOfBoundsException.class)
    public void excessiveSetIndex() {
        IArray array = new NaiveArray(5);
        array.set(5, 21.0);
        Assert.fail();
    }

    @Test(expectedExceptions = ArrayIndexOutOfBoundsException.class)
    public void negativeGetIndex() {
        IArray array = new NaiveArray(5);
        array.get(-1);
        Assert.fail();
    }

    @Test(expectedExceptions = ArrayIndexOutOfBoundsException.class)
    public void excessiveGetIndex() {
        IArray array = new NaiveArray(5);
        array.get(100);
        Assert.fail();
    }

    @Test
    public void capacity() {
        IArray array = new NaiveArray(5);
        Assert.assertEquals(array.capacity(), 5);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void negativeCapacity() {
        IArray array = new NaiveArray(-5);
        Assert.fail();
    }

    @Test
    public void used() {
        IArray array = new NaiveArray(5);
        Assert.assertEquals(array.size(), 5);
    }

    @Test
    public void empty() {
        IArray array = new NaiveArray(5);
        Assert.assertFalse(array.isEmpty());
    }

    @Test
    public void string() {
        IArray array = new NaiveArray(3);
        array.set(0, 77.0);
        array.set(1, 88.0);
        array.set(2, 99.0);
        String s = array.toString();
        Assert.assertTrue(s.contains("77"));
        Assert.assertTrue(s.contains("88"));
        Assert.assertTrue(s.contains("99"));
    }

}