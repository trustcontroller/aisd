package com.portifacto.aisd;

import org.testng.Assert;
import org.testng.annotations.Test;

public class ExpandableStorageTest {

    @Test
    public void defaultStorage() {
        ExpandableStorage es = new ExpandableStorage();
        Assert.assertTrue(es.length() > 0);
    }

    @Test
    public void zeroCapacity() {
        ExpandableStorage es = new ExpandableStorage(0);
        Assert.assertTrue(es.length() == 0);
    }

    @Test
    public void arrayExpandedJustBelow() {
        ExpandableStorage es = new ExpandableStorage(10);
        es.incrementCapacity(19);
        Assert.assertTrue(es.length() > 19);
    }

    @Test
    public void arrayExpandedJustAt() {
        ExpandableStorage es = new ExpandableStorage(10);
        es.incrementCapacity(20);
        Assert.assertTrue(es.length() > 20);
    }

    @Test
    public void arrayExpandedJustAbove() {
        ExpandableStorage es = new ExpandableStorage(10);
        es.incrementCapacity(21);
        Assert.assertTrue(es.length() > 21);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void negativeCapacity() {
        ExpandableStorage es = new ExpandableStorage(-3);
        Assert.fail();

    }


}