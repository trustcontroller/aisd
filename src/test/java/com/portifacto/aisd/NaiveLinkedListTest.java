package com.portifacto.aisd;

import org.testng.Assert;
import org.testng.annotations.Test;

public class NaiveLinkedListTest extends TestHelper {

    @Test
    public void emptyList() {
        IList list = new NaiveLinkedList();
        Assert.assertTrue(list.isEmpty());
        Assert.assertEquals(list.size(), 0);
        Assert.assertEquals(list.capacity(), Integer.MAX_VALUE);
    }

    @Test
    public void findOnEmptyList() {
        IList list = new NaiveLinkedList();
        boolean found = list.find(12.0);
        Assert.assertFalse(found);
    }

    @Test
    public void findExisting() {
        IList list = new NaiveLinkedList();
        list.add(10.0);
        boolean found = list.find(10.0);
        Assert.assertTrue(found);
    }

    @Test
    public void findNotExisting() {
        IList list = new NaiveLinkedList();
        list.add(10.0);
        boolean found = list.find(11.0);
        Assert.assertFalse(found);
    }

    @Test
    public void removeCentral() {
        IList list = new NaiveLinkedList();
        list.add(10.0);
        list.add(20.0);
        list.add(30.0);

        list.remove(20.0);

        Assert.assertTrue(identical(list, new double[]{30.0, 10.0}));
    }

    @Test
    public void removeLast() {
        IList list = new NaiveLinkedList();
        list.add(10.0);
        list.add(20.0);
        list.add(30.0);

        list.remove(10.0);

        Assert.assertTrue(identical(list, new double[]{30.0, 20.0}));
    }

    @Test
    public void removeFirst() {
        IList list = new NaiveLinkedList();
        list.add(10.0);
        list.add(20.0);
        list.add(30.0);

        list.remove(30.0);

        Assert.assertTrue(identical(list, new double[]{20.0, 10.0}));
    }

    @Test
    public void twoIdenticalValues() {
        IList list = new NaiveLinkedList();
        list.add(10.0);
        list.add(10.0);

        Assert.assertTrue(identical(list, new double[]{10.0, 10.0}));
    }

    @Test
    public void string() {
        IList list = new NaiveLinkedList();
        list.add(77.0);
        list.add(88.0);
        String s = list.toString();
        Assert.assertTrue(s.contains("77"));
        Assert.assertTrue(s.contains("88"));
    }

    @Test
    public void iterator() {
        IList list = new NaiveLinkedList();
        list.add(77.0);
        list.add(88.0);

        IIterator iterator = list.iterator();
        Double x;
        while ((x = iterator.get()) != null) {
            System.out.print(" " + x);
        }
        System.out.println();

    }

}