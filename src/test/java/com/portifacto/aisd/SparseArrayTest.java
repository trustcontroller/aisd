package com.portifacto.aisd;

import org.testng.Assert;
import org.testng.annotations.Test;

public class SparseArrayTest {

    @Test
    public void nominalInOrder() {
        IArray s = new SparseArray(10);

        s.set(100, 20.0);
        s.set(110, 30.0);

        Assert.assertEquals(s.size(), 110);

        double d1 = s.get(100);
        double d2 = s.get(110);
        Assert.assertEquals(d1, 20.0);
        Assert.assertEquals(d2, 30.0);
    }

    @Test
    public void nominalReverseOrder() {
        IArray s = new SparseArray(10);

        s.set(110, 30.0);
        s.set(100, 20.0);

        double d1 = s.get(100);
        double d2 = s.get(110);
        Assert.assertEquals(d1, 20.0);
        Assert.assertEquals(d2, 30.0);
    }

    @Test
    public void capacityIncrement() {
        IArray s = new SparseArray(3);

        s.set(21, 20.0);
        s.set(31, 30.0);
        s.set(41, 40.0);
        s.set(51, 50.0);

        Assert.assertEquals(s.get(21), 20.0);
        Assert.assertEquals(s.get(31), 30.0);
        Assert.assertEquals(s.get(41), 40.0);
        Assert.assertEquals(s.get(51), 50.0);
    }

    @Test
    public void capacity() {
        IArray array = new SparseArray(3);
        Assert.assertEquals(array.capacity(), Integer.MAX_VALUE);
        Assert.assertFalse(array.isEmpty());
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void negativeCapacity() {
        IArray s = new SparseArray(-1);
        Assert.fail();
    }

    @Test
    public void twiceIdenticalValue() {
        IArray s = new SparseArray();

        s.set(100, 20.0);
        s.set(110, 20.0);

        double d1 = s.get(100);
        double d2 = s.get(110);
        Assert.assertEquals(d1, 20.0);
        Assert.assertEquals(d2, 20.0);
    }

    @Test
    public void overwriteValue() {
        IArray s = new SparseArray();

        s.set(100, 20.0);
        s.set(100, 30.0);

        double d1 = s.get(100);
        Assert.assertEquals(d1, 30.0);
    }

    @Test(expectedExceptions = ArrayIndexOutOfBoundsException.class)
    public void negativeIndexSet() {
        IArray array = new SparseArray(3);
        array.set(-2, 20.0);
        Assert.fail();
    }

    @Test(expectedExceptions = ArrayIndexOutOfBoundsException.class)
    public void negativeIndexGet() {
        IArray array = new SparseArray(3);
        array.get(-2);
        Assert.fail();
    }

    @Test
    public void getNonExisting() {
        IArray array = new SparseArray(3);
        double x = array.get(50);
        Assert.assertEquals(x, 0.0);
    }

    @Test
    public void getAbovePreviouslySet() {
        IArray array = new SparseArray(3);
        array.set(100, 10.0);
        double x = array.get(200);
        Assert.assertEquals(x, 0.0);
    }

    @Test
    public void getBelowPreviouslySet() {
        IArray array = new SparseArray(3);
        array.set(100, 10.0);
        double x = array.get(50);
        Assert.assertEquals(x, 0.0);
    }


}