package com.portifacto.aisd;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.Random;

public class HashSetTest extends TestHelper {

    @Test
    public void empty() {
        HashSet hashSet = new HashSet();
        Assert.assertTrue(hashSet.getAsList().isEmpty());
        Assert.assertEquals(hashSet.capacity(), HashSet.DEFAULT_CAPACITY);
    }

    @Test
    public void oneElement() {
        HashSet hashSet = new HashSet();
        hashSet.add(6.0);
        Assert.assertTrue(identical(hashSet.getAsList(), new double[]{6.0}));
    }

    @Test
    public void forceResize() {
        HashSet hashSet = new HashSet();
        int capacity = hashSet.capacity();
        for (int i = 0; i <= capacity; i++) {
            hashSet.add(i);
        }
        Assert.assertEquals(hashSet.capacity(),
                HashSet.DEFAULT_CAPACITY + HashSet.INCREMENTAL_CAPACITY);
    }

    @Test
    public void positionsZeroAndTwo() {
        HashSet hashSet = new HashSet();
        hashSet.add(1.0);
        hashSet.add(4.0);
        Assert.assertTrue(hashSet.getAsList().find(1.0));
        Assert.assertTrue(hashSet.getAsList().find(4.0));
    }

    @Test
    public void collisionAtFive() {
        HashSet hashSet = new HashSet();
        hashSet.add(5.0);
        hashSet.add(13.0);
        Assert.assertTrue(hashSet.getAsList().find(5.0));
        Assert.assertTrue(hashSet.getAsList().find(13.0));
    }

    @Test
    public void collisionAtTenWithWrap() {
        HashSet hashSet = new HashSet();
        hashSet.add(9.0);
        hashSet.add(24.0);
        Assert.assertTrue(hashSet.getAsList().find(9.0));
        Assert.assertTrue(hashSet.getAsList().find(24.0));
    }

    @Test
    public void findInEmpty() {
        HashSet hashSet = new HashSet();
        Assert.assertFalse(hashSet.find(2.0));
    }

    @Test
    public void findInOneElement() {
        HashSet hashSet = new HashSet();
        hashSet.add(2.0);
        Assert.assertTrue(hashSet.find(2.0));
    }

    @Test
    public void findInSimulatedFull() {
        HashSet hashSet = new HashSet();
        Double[] data = new Double[]{1.0, 2.0, 3.0, 4.0, 5.0};
        Assert.assertFalse(hashSet.find(99.0, data));
    }

    @Test
    public void findNonExistingInOneElement() {
        HashSet hashSet = new HashSet();
        hashSet.add(2.0);
        Assert.assertFalse(hashSet.find(3.0));
    }

    @Test
    public void findAfterCollisionAtTenWithWrap() {
        HashSet hashSet = new HashSet();
        hashSet.add(9.0);
        hashSet.add(24.0);
        Assert.assertTrue(hashSet.find(9.0));
        Assert.assertTrue(hashSet.find(24.0));
    }

    @Test
    public void string() {
        HashSet hashSet = new HashSet();
        hashSet.add(99.0);
        hashSet.add(88.0);
        String s = hashSet.toString();
        Assert.assertTrue(s.contains("88"));
        Assert.assertTrue(s.contains("99"));
    }

    @Test
    public void iteratorOnEmpty() {
        HashSet hashSet = new HashSet();
        IIterator iterator = hashSet.iterator();
        Double value = iterator.get();
        Assert.assertNull(value);
    }

    @Test
    public void iteratorOnOne() {
        HashSet hashSet = new HashSet();
        hashSet.add(22.2);
        IIterator iterator = hashSet.iterator();
        Double value = iterator.get();
        Assert.assertEquals(value.doubleValue(), 22.2, 0.001);
    }

    @Test
    public void iteratorOnTwo() {
        HashSet hashSet = new HashSet();
        hashSet.add(22.0);
        hashSet.add(33.0);
        IIterator iterator = hashSet.iterator();
        Double v1 = iterator.get();
        Double v2 = iterator.get();
        Double v3 = iterator.get();
        Assert.assertTrue(v1.doubleValue() == 22.0 || v2.doubleValue() == 22.0);
        Assert.assertTrue(v1.doubleValue() == 33.0 || v2.doubleValue() == 33.0);
        Assert.assertNull(v3);
    }

    // for demonstration only
    public void checkPrimeImperfectHash() {
        int eleven[] = new int[11];
        for (int i = 0; i < eleven.length; i++) eleven[i] = 0;
        int twelve[] = new int[12];
        for (int i = 0; i < twelve[i]; i++) twelve[i] = 0;

        for (int i = 0; i < 1000; i++) {
            int hash = Double.valueOf(i).hashCode();
            eleven[hash % 11]++;
            twelve[hash % 12]++;
        }
        System.out.println("----");
        for (int i = 0; i < eleven.length; i++) System.out.println(i + " -- " + eleven[i]);
        System.out.println("----");
        for (int i = 0; i < twelve.length; i++) System.out.println(i + " -- " + twelve[i]);
        System.out.println("----");
    }

    // for demonstration only
    public void loadFactor() {
        int CAPACITY = 11;
        int ITERATIONS = 100;
        Random random = new Random();
        long[] steps = new long[CAPACITY];
        long[] fails = new long[CAPACITY];
        for (int iteration = 0; iteration < ITERATIONS; iteration++) {
            Double[] array = new Double[CAPACITY];
            for (int i = 0; i < array.length; i++) array[i] = null;
            for (int j = 0; j < CAPACITY; j++) {

                Double value = Double.valueOf(random.nextDouble());

                int position = Math.abs(value.hashCode()) % CAPACITY;
                boolean inserted = false;
                steps[j]++;
                if (array[position] == null) {
                    array[position] = value;
                    inserted = true;
                } else {
                    for (int i = 1; i < CAPACITY; i++) {
                        steps[j]++;
                        int potential = (position + i) % CAPACITY;
                        if (array[potential] == null) {
                            array[potential] = value;
                            inserted = true;
                            break;
                        }
                    }
                }
                if (!inserted) {
                    fails[j]++;
                }
            }
        }

        for (int i = 0; i < CAPACITY; i++) {
            System.out.println(" " + i + "   " + (double) steps[i] / ITERATIONS + " " + (double) fails[i] / ITERATIONS);
        }
    }

}