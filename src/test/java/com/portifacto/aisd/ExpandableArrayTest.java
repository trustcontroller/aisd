package com.portifacto.aisd;

import org.testng.Assert;
import org.testng.annotations.Test;

public class ExpandableArrayTest {

    private static final double EPSILON = 0.0001;

    @Test
    public void defaultArray() {
        IArray array = new ExpandableArray();
        array.set(4, 20.0);
        Assert.assertEquals(array.get(4), 20, EPSILON);
    }

    @Test
    public void zeroCapacity() {
        IArray array = new ExpandableArray(0);
        array.set(4, 20.0);
        Assert.assertEquals(array.get(4), 20, EPSILON);
    }

    @Test
    public void arrayExpandedJustAt() {
        IArray array = new ExpandableArray(3);
        array.set(20, 20.0);
        Assert.assertEquals(array.get(20), 20, EPSILON);
    }

    @Test
    public void arrayExpandedJustAbove() {
        IArray array = new ExpandableArray(3);
        array.set(21, 20.0);
        Assert.assertEquals(array.get(21), 20, EPSILON);
    }

    @Test
    public void arrayExpandedJustBelow() {
        IArray array = new ExpandableArray(3);
        array.set(19, 20.0);
        Assert.assertEquals(array.get(19), 20, EPSILON);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void negativeCapacity() {
        new ExpandableArray(-3);
        Assert.fail();
    }

    @Test
    public void used() {
        IArray array = new ExpandableArray(3);
        array.set(19, 20.0);
        Assert.assertEquals(array.size(), 19);
    }

    @Test(expectedExceptions = ArrayIndexOutOfBoundsException.class)
    public void negativeIndexSet() {
        IArray array = new ExpandableArray(3);
        array.set(-2, 20.0);
        Assert.fail();
    }

    @Test(expectedExceptions = ArrayIndexOutOfBoundsException.class)
    public void negativeIndexGet() {
        IArray array = new ExpandableArray(3);
        array.get(-2);
        Assert.fail();
    }

    @Test
    public void getNotPreviouslySet() {
        IArray array = new ExpandableArray(3);
        double x = array.get(200);
        Assert.assertEquals(x, 0.0);
    }

    @Test
    public void capacity() {
        IArray array = new ExpandableArray(3);
        Assert.assertEquals(array.capacity(), Integer.MAX_VALUE);
    }

    @Test
    public void empty() {
        IArray array = new ExpandableArray(3);
        Assert.assertFalse(array.isEmpty());
    }

    @Test
    public void string() {
        IArray array = new ExpandableArray(3);
        array.set(0, 77.0);
        array.set(1, 88.0);
        array.set(2, 99.0);
        String s = array.toString();
        Assert.assertTrue(s.contains("77"));
        Assert.assertTrue(s.contains("88"));
        Assert.assertTrue(s.contains("99"));
    }

}