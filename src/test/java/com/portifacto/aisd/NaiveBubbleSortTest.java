package com.portifacto.aisd;

import org.testng.Assert;
import org.testng.annotations.Test;

public class NaiveBubbleSortTest extends TestHelper {

    @Test
    public void randomNumbers() {
        double[] vector = {4.0, 3.0, 2.0, 6.0};
        double[] result = new NaiveBubbleSort().sort(vector, vector.length);
        Assert.assertTrue(identical(result, new double[]{2.0, 3.0, 4.0, 6.0}));
    }

    @Test
    public void totalInversion() {
        double[] vector = {4.0, 3.0, 2.0, 1.0};
        double[] result = new NaiveBubbleSort().sort(vector, vector.length);
        Assert.assertTrue(identical(result, new double[]{1.0, 2.0, 3.0, 4.0}));
    }

    @Test
    public void orderPreserved() {
        double[] vector = {1.0, 2.0, 3.0, 4.0};
        double[] result = new NaiveBubbleSort().sort(vector, vector.length);
        Assert.assertTrue(identical(result, new double[]{1.0, 2.0, 3.0, 4.0}));
    }

    @Test
    public void zeroLength() {
        double[] vector = {};
        double[] result = new NaiveBubbleSort().sort(vector, vector.length);
        Assert.assertTrue(identical(result, new double[]{}));
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void nullInput() {
        double[] vector = null;
        double[] result = new NaiveBubbleSort().sort(vector, 0);
        Assert.fail();
    }


}