package com.portifacto.aisd;

import org.testng.Assert;
import org.testng.annotations.Test;

public class TraversalTest {

    @Test
    public void traversal() {
        IGraph graph = new ArrayGraph();
        graph.addNode("A");
        graph.addNode("B");
        graph.addNode("C");
        graph.addNode("D");
        graph.addNode("E");

        graph.addDirectedEdge("A", "D");
        graph.addDirectedEdge("D", "C");
        graph.addDirectedEdge("D", "B");
        graph.addDirectedEdge("C", "B");
        graph.addDirectedEdge("C", "A");
        graph.addDirectedEdge("E", "D");

        Traversal traversal = new Traversal(graph);
        String[] result = traversal.traversal();
        Assert.assertEquals(result.length, 5);
        Assert.assertEquals(result[0], "A");
        Assert.assertEquals(result[1], "D");
        Assert.assertEquals(result[2], "B");
        Assert.assertEquals(result[3], "C");
        Assert.assertEquals(result[4], "E");

    }


}