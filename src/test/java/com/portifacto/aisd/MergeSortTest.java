package com.portifacto.aisd;

import org.testng.Assert;
import org.testng.annotations.Test;

public class MergeSortTest extends TestHelper {

    @Test
    public void evenNumbers() {
        double[] vector = {4.0, 3.0, 2.0, 6.0};
        double[] result = new MergeSort().sort(vector);
        Assert.assertTrue(identical(result, new double[]{2.0, 3.0, 4.0, 6.0}));
    }

    @Test
    public void oddNumbers() {
        double[] vector = {4.0, 3.0, 2.0, 6.0, 1.0};
        double[] result = new MergeSort().sort(vector, vector.length);
        Assert.assertTrue(identical(result, new double[]{1.0, 2.0, 3.0, 4.0, 6.0}));
    }

    @Test
    public void totalInversion() {
        double[] vector = {4.0, 3.0, 2.0, 1.0};
        double[] result = new MergeSort().sort(vector, vector.length);
        Assert.assertTrue(identical(result, new double[]{1.0, 2.0, 3.0, 4.0}));
    }

    @Test
    public void orderPreserved() {
        double[] vector = {1.0, 2.0, 3.0, 4.0};
        double[] result = new MergeSort().sort(vector, vector.length);
        Assert.assertTrue(identical(result, new double[]{1.0, 2.0, 3.0, 4.0}));
    }

    @Test
    public void zeroLength() {
        double[] vector = {};
        double[] result = new MergeSort().sort(vector, vector.length);
        Assert.assertTrue(identical(result, new double[]{}));
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void nullInput() {
        double[] vector = null;
        double[] result = new MergeSort().sort(vector, 0);
        Assert.fail();
    }


}