package com.portifacto.aisd;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.concurrent.atomic.AtomicInteger;

public class PermutationsTest {

    @Test
    public void numberOfPermutation() {
        AtomicInteger count = new AtomicInteger();
        Permutations permutations = new Permutations(3,
                perm -> count.incrementAndGet());
        permutations.generate();
        Assert.assertEquals(count.get(), 6);
    }

    @Test
    public void checkPermutationsAnyOrder() {
        int[][] expected = new int[][]
                {{0, 1, 2}, {0, 2, 1}, {1, 0, 2}, {1, 2, 0}, {2, 0, 1}, {2, 1, 0}};
        boolean[] found = new boolean[]
                {false, false, false, false, false, false};
        Permutations permutations = new Permutations(3,
                perm -> {
                    for (int i = 0; i < expected.length; i++) {
                        boolean match = true;
                        for (int j = 0; j < expected[i].length; j++) {
                            if (expected[i][j] != perm[j]) {
                                match = false;
                                break;
                            }
                        }
                        if (match) found[i] = true;
                    }
                });
        permutations.generate();
        for (boolean f : found) if (!f) Assert.fail();
    }


}