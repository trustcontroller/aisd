package com.portifacto.aisd;

import org.testng.Assert;
import org.testng.annotations.Test;

public class FibonacciArrayTest {

    @Test
    public void zero() {
        IArray array = new FibonacciArray();
        Assert.assertEquals(0.0, array.get(0));
    }

    @Test
    public void one() {
        IArray array = new FibonacciArray();
        Assert.assertEquals(1.0, array.get(1));
    }

    @Test
    public void two() {
        IArray array = new FibonacciArray();
        Assert.assertEquals(1.0, array.get(2));
    }

    @Test
    public void three() {
        IArray array = new FibonacciArray();
        Assert.assertEquals(2.0, array.get(3));
    }

    @Test
    public void four() {
        IArray array = new FibonacciArray();
        Assert.assertEquals(3.0, array.get(4));
    }

    @Test
    public void size() {
        IArray array = new FibonacciArray();
        Assert.assertTrue(array.size() < 100);
    }

    @Test
    public void capacity() {
        IArray array = new FibonacciArray();
        Assert.assertTrue(array.capacity() < 100);
    }

    @Test
    public void immutableSet() {
        IArray array = new FibonacciArray();
        array.set(0, 777.0);
        Assert.assertEquals(array.get(0), 0.0);
    }

    @Test(expectedExceptions = ArrayIndexOutOfBoundsException.class)
    public void invalidIndexSet() {
        IArray array = new FibonacciArray();
        array.set(-1, 0.0);
        Assert.fail();
    }

    @Test(expectedExceptions = ArrayIndexOutOfBoundsException.class)
    public void invalidIndexGet() {
        IArray array = new FibonacciArray();
        array.get(-1);
        Assert.fail();
    }

    @Test
    public void empty() {
        IArray array = new FibonacciArray();
        Assert.assertFalse(array.isEmpty());
    }

}