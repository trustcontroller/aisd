package com.portifacto.aisd;

import org.testng.Assert;
import org.testng.annotations.Test;

public class ColouringTest {

    @Test
    public void colouringPetersenGraph() {
        IGraph graph = buildPetersenGraph();

        Colouring colouring = new Colouring(graph);
        Assert.assertTrue(colouring.colouring(3));
        int[] result = colouring.getResult();
        Assert.assertNotNull(result);
        Assert.assertEquals(result.length, graph.getNodeCount());

        for (int i = 0; i < graph.getNodeCount(); i++) {
            for (int j = 0; j < graph.getNodeCount(); j++) {
                if (graph.isEdgePresent(i, j)) {
                    if (result[i] == result[j]) {
                        Assert.fail();
                    }
                }
            }
        }
    }

    @Test
    public void impossibleColouring() {
        IGraph graph = buildPetersenGraph();

        Colouring colouring = new Colouring(graph);
        boolean result = colouring.colouring(2);

        Assert.assertFalse(result);

    }

    private IGraph buildPetersenGraph() {

        IGraph graph = new ArrayGraph();
        graph.addNode("0");
        graph.addNode("1");
        graph.addNode("2");
        graph.addNode("3");
        graph.addNode("4");
        graph.addNode("5");
        graph.addNode("6");
        graph.addNode("7");
        graph.addNode("8");
        graph.addNode("9");

        graph.addUndirectedEdge("0", "1");
        graph.addUndirectedEdge("1", "2");
        graph.addUndirectedEdge("2", "3");
        graph.addUndirectedEdge("3", "4");
        graph.addUndirectedEdge("4", "0");

        graph.addUndirectedEdge("0", "5");
        graph.addUndirectedEdge("1", "6");
        graph.addUndirectedEdge("2", "6");
        graph.addUndirectedEdge("3", "8");
        graph.addUndirectedEdge("4", "9");

        graph.addUndirectedEdge("5", "7");
        graph.addUndirectedEdge("5", "8");
        graph.addUndirectedEdge("6", "8");
        graph.addUndirectedEdge("6", "9");
        graph.addUndirectedEdge("7", "9");

        return graph;

    }

}