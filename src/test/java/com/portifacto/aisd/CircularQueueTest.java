package com.portifacto.aisd;

import org.testng.Assert;
import org.testng.annotations.Test;

public class CircularQueueTest {

    @Test
    public void initiallyEmpty() {
        IQueue queue = new CircularQueue(3);
        Assert.assertTrue(queue.isEmpty());
        Double result = queue.poll();
        Assert.assertNull(result);
        Assert.assertEquals(queue.size(), 0);
        Assert.assertEquals(queue.capacity(), 3);
    }

    @Test
    public void defaultCapacity() {
        IQueue queue = new CircularQueue();
        Assert.assertEquals(queue.capacity(), CircularQueue.DEFAULT_SIZE);
    }

    @Test
    public void addAndRemoveOne() {
        IQueue queue = new CircularQueue(3);
        boolean r1 = queue.offer(22.2);
        Assert.assertTrue(r1);
        Double r2 = queue.poll();
        Assert.assertNotNull(r2);
        Assert.assertEquals(22.2, r2.doubleValue(), 0.001);
    }

    @Test
    public void overload() {
        IQueue queue = new CircularQueue(3);
        Assert.assertTrue(queue.offer(22.2));
        Assert.assertTrue(queue.offer(33.3));
        Assert.assertTrue(queue.offer(44.4));
        Assert.assertFalse(queue.offer(55.5));

    }

    @Test
    public void rollOver() {
        IQueue queue = new CircularQueue(3);
        Assert.assertTrue(queue.offer(22.2));
        Assert.assertTrue(queue.offer(33.3));
        Assert.assertTrue(queue.offer(44.4));
        Assert.assertNotNull(queue.poll());
        Assert.assertTrue(queue.offer(55.5));
        Assert.assertEquals(33.3, queue.poll().doubleValue(), 0.001);
        Assert.assertEquals(44.4, queue.poll().doubleValue(), 0.001);
        Assert.assertEquals(55.5, queue.poll().doubleValue(), 0.001);

    }

    @Test
    public void exhaustToEmpty() {
        IQueue queue = new CircularQueue(3);
        Assert.assertTrue(queue.offer(22.2));
        Assert.assertTrue(queue.offer(33.3));
        Assert.assertNotNull(queue.poll());
        Assert.assertNotNull(queue.poll());
        Assert.assertNull(queue.poll());
    }

    @Test
    public void nonEmptyUsed() {
        IQueue queue = new CircularQueue(3);
        queue.offer(22.2);
        queue.offer(33.3);
        Assert.assertEquals(queue.size(), 2);

    }

}