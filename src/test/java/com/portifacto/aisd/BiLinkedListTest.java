package com.portifacto.aisd;

import org.testng.Assert;
import org.testng.annotations.Test;

public class BiLinkedListTest extends TestHelper {

    @Test
    public void empty() {
        IList list = new BiLinkedList();
        Assert.assertTrue(list.isEmpty());
    }

    @Test
    public void findExisting() {
        IList list = new BiLinkedList();
        list.add(88.0);
        Assert.assertTrue(list.find(88.0));
    }

    @Test
    public void findNonExisting() {
        IList list = new BiLinkedList();
        list.add(88.0);
        Assert.assertFalse(list.find(99.0));
    }

    @Test
    public void removeFirstMiddleLast() {
        IList list = new BiLinkedList();
        list.add(22.0);
        list.add(33.0);
        list.add(44.0);
        list.add(55.0);
        list.add(66.0);
        list.remove(22.0);
        list.remove(44.0);
        list.remove(66.0);
        Assert.assertTrue(identical(list, new double[]{33.0, 55.0}));
    }

    @Test
    public void removeAll() {
        IList list = new BiLinkedList();
        list.add(22.0);
        list.add(33.0);
        list.remove(22.0);
        list.remove(33.0);
        Assert.assertTrue(list.isEmpty());
    }

    @Test
    public void reverseTraversal() {
        BiLinkedList list = new BiLinkedList();
        list.add(22.0);
        list.add(33.0);
        IIterator iterator = list.reverseIterator();
        Assert.assertEquals(iterator.get().doubleValue(), 33.0);
        Assert.assertEquals(iterator.get().doubleValue(), 22.0);
        Assert.assertNull(iterator.get());
    }

    @Test
    public void size() {
        BiLinkedList list = new BiLinkedList();
        list.add(22.0);
        list.add(33.0);
        Assert.assertEquals(list.size(), 2);
    }

    @Test
    public void capacity() {
        BiLinkedList list = new BiLinkedList();
        Assert.assertEquals(list.capacity(), Integer.MAX_VALUE);
    }

    @Test
    public void string() {
        IList list = new BiLinkedList();
        list.add(77.0);
        list.add(88.0);
        String s = list.toString();
        Assert.assertTrue(s.contains("77"));
        Assert.assertTrue(s.contains("88"));
    }


}