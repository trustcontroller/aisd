package com.portifacto.aisd;

import org.testng.Assert;
import org.testng.annotations.Test;

public class ArrayGraphTest {

    @Test
    public void emptyGraph() {
        ArrayGraph graph = new ArrayGraph();
        Assert.assertEquals(graph.getNodeCount(), 0);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void addNullName() {
        ArrayGraph graph = new ArrayGraph();
        graph.addNode(null);
        Assert.fail();
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void addNullEdgeDirectional() {
        ArrayGraph graph = new ArrayGraph();
        graph.addNode("A");
        graph.addNode("B");
        graph.addDirectedEdge(null, null);
        Assert.fail();
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void addEdgeDirectionalNonExistingNode() {
        ArrayGraph graph = new ArrayGraph();
        graph.addNode("A");
        graph.addNode("B");
        graph.addDirectedEdge("X", "Y");
        Assert.fail();
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void addEdgeUnDirectionalNonExistingNode() {
        ArrayGraph graph = new ArrayGraph();
        graph.addNode("A");
        graph.addNode("B");
        graph.addUndirectedEdge("X", "Y");
        Assert.fail();
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void addNullEdgeUndirected() {
        ArrayGraph graph = new ArrayGraph();
        graph.addNode("A");
        graph.addNode("B");
        graph.addUndirectedEdge(null, null);
        Assert.fail();
    }

    @Test
    public void getNonExistingNode() {
        ArrayGraph graph = new ArrayGraph();
        graph.addNode("Ala");
        Integer index = graph.getNodeIndex("As");
        Assert.assertNull(index);
    }

    @Test
    public void twoNodesDirected() {
        ArrayGraph graph = new ArrayGraph();
        graph.addNode("Ala");
        graph.addNode("As");
        int a1 = graph.getNodeIndex("Ala");
        int a2 = graph.getNodeIndex("As");
        graph.addDirectedEdge("Ala", "As", 12.0);
        Assert.assertEquals(graph.getNodeCount(), 2);
        Assert.assertEquals(graph.getNodeName(a1), "Ala");
        Assert.assertEquals(graph.getNodeName(a2), "As");
        Assert.assertTrue(graph.isEdgePresent(a1, a2));
        Assert.assertEquals(graph.getEdgeWeight(a1, a2), 12.0, 0.0001);
    }

    @Test
    public void twoNodesUndirected() {
        ArrayGraph graph = new ArrayGraph();
        graph.addNode("Ala");
        graph.addNode("As");
        graph.addUndirectedEdge("Ala", "As", 12.0);
        int a1 = graph.getNodeIndex("Ala");
        int a2 = graph.getNodeIndex("As");
        Assert.assertEquals(graph.getNodeCount(), 2);
        Assert.assertEquals(graph.getNodeName(a1), "Ala");
        Assert.assertEquals(graph.getNodeName(a2), "As");
        Assert.assertTrue(graph.isEdgePresent(a1, a2));
        Assert.assertTrue(graph.isEdgePresent(a2, a1));
        Assert.assertEquals(graph.getEdgeWeight(a1, a2), 12.0, 0.0001);
        Assert.assertEquals(graph.getEdgeWeight(a2, a1), 12.0, 0.0001);
    }

    @Test
    public void twoNodesUndirectedNoWeight() {
        ArrayGraph graph = new ArrayGraph();
        graph.addNode("Ala");
        graph.addNode("As");
        graph.addUndirectedEdge("Ala", "As");
        int a1 = graph.getNodeIndex("Ala");
        int a2 = graph.getNodeIndex("As");
        Assert.assertTrue(graph.isEdgePresent(a1, a2));
        Assert.assertTrue(graph.isEdgePresent(a2, a1));
        Assert.assertNull(graph.getEdgeWeight(a1, a2));
        Assert.assertNull(graph.getEdgeWeight(a2, a1));
    }

    @Test
    public void twoNodesDirectedNoWeight() {
        ArrayGraph graph = new ArrayGraph();
        graph.addNode("Ala");
        graph.addNode("As");
        graph.addDirectedEdge("Ala", "As");
        int a1 = graph.getNodeIndex("Ala");
        int a2 = graph.getNodeIndex("As");
        Assert.assertTrue(graph.isEdgePresent(a1, a2));
        Assert.assertFalse(graph.isEdgePresent(a2, a1));
        Assert.assertNull(graph.getEdgeWeight(a1, a2));
        Assert.assertNull(graph.getEdgeWeight(a2, a1));
    }

    @Test
    public void twoNodesBidirected() {
        ArrayGraph graph = new ArrayGraph();
        graph.addNode("Ala");
        graph.addNode("As");
        graph.addBiDirectedEdge("Ala", "As", 12.0, 10.0);
        int a1 = graph.getNodeIndex("Ala");
        int a2 = graph.getNodeIndex("As");
        Assert.assertEquals(graph.getEdgeWeight(a1, a2), 12.0, 0.0001);
        Assert.assertEquals(graph.getEdgeWeight(a2, a1), 10.0, 0.0001);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void identicalNames() {
        ArrayGraph graph = new ArrayGraph();
        graph.addNode("Ala");
        graph.addNode("Ala");
        Assert.fail();

    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void redefineDirectedEdge() {
        ArrayGraph graph = new ArrayGraph();
        graph.addNode("Ala");
        graph.addNode("As");
        graph.addDirectedEdge("Ala", "As", 12.0);
        graph.addDirectedEdge("Ala", "As", 10.0);
        Assert.fail();

    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void redefineUndirectedEdgeFrom() {
        ArrayGraph graph = new ArrayGraph();
        graph.addNode("Ala");
        graph.addNode("As");
        graph.addDirectedEdge("Ala", "As", 12.0);
        graph.addUndirectedEdge("Ala", "As", 10.0);
        Assert.fail();

    }

    @Test
    public void nodeIndexNegative() {
        ArrayGraph graph = new ArrayGraph();
        String name = graph.getNodeName(-1);
        Assert.assertNull(name);
    }

    @Test
    public void nodeIndexTooLarge() {
        ArrayGraph graph = new ArrayGraph();
        String name = graph.getNodeName(12);
        Assert.assertNull(name);
    }

    @Test
    public void edgeIndexNegative() {
        ArrayGraph graph = new ArrayGraph();
        Assert.assertFalse(graph.isEdgePresent(-1, 0));
    }

    @Test
    public void edgeIndexTooLarge() {
        ArrayGraph graph = new ArrayGraph();
        Assert.assertFalse(graph.isEdgePresent(0, 10));
    }

    @Test
    public void edgeWeightIndexNegative() {
        ArrayGraph graph = new ArrayGraph();
        Assert.assertNull(graph.getEdgeWeight(-1, 0));
    }

    @Test
    public void edgeWeightIndexTooLarge() {
        ArrayGraph graph = new ArrayGraph();
        Assert.assertNull(graph.getEdgeWeight(0, 10));
    }

    @Test
    public void nodeWeightCorrect() {
        ArrayGraph graph = new ArrayGraph();
        graph.addNode("Ala", 12.0);
        Assert.assertEquals(graph.getNodeWeight(graph.getNodeIndex("Ala")).doubleValue(), 12.0);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void redefineUndirectedEdgeTo() {
        ArrayGraph graph = new ArrayGraph();
        graph.addNode("Ala");
        graph.addNode("As");
        graph.addDirectedEdge("As", "Ala", 12.0);
        graph.addUndirectedEdge("Ala", "As", 10.0);
        Assert.fail();

    }

    @Test
    public void string() {
        ArrayGraph graph = new ArrayGraph();
        graph.addNode("Ala", 88.0);
        graph.addNode("As");
        graph.addDirectedEdge("As", "Ala", 99.0);
        graph.addDirectedEdge("Ala", "As");
        String s = graph.toString();
        Assert.assertTrue(s.contains("Ala"));
        Assert.assertTrue(s.contains("As"));
        Assert.assertTrue(s.contains("88"));
        Assert.assertTrue(s.contains("99"));
    }
}