package com.portifacto.aisd;

public class TestHelper {

    protected boolean identical(IList list, double[] expected) {
        IIterator iterator = list.iterator();
        return identical(iterator, expected);
    }

    protected boolean identical(IIterator iterator, double[] expected) {
        Double x;
        int i = 0;
        while ((x = iterator.get()) != null) {
            if (i >= expected.length) return false;
            if (x != expected[i]) return false;
            i++;
        }
        if (i < expected.length) return false;
        return true;
    }

    protected boolean identical(double[] actual, double[] expected) {
        if (actual == null || expected == null) return false;
        if (actual.length != expected.length) return false;
        for (int i = 0; i < expected.length; i++) {
            if (actual[i] != expected[i]) return false;
        }
        return true;
    }

    protected boolean identical(int[] actual, int[] expected) {
        if (actual == null || expected == null) return false;
        if (actual.length != expected.length) return false;
        for (int i = 0; i < expected.length; i++) {
            if (actual[i] != expected[i]) return false;
        }
        return true;
    }

    protected boolean identical(String[] actual, String[] expected) {
        if (actual == null || expected == null) return false;
        if (actual.length != expected.length) return false;
        for (int i = 0; i < expected.length; i++) {
            if (actual[i].compareTo(expected[i]) != 0) return false;
        }
        return true;
    }

}
