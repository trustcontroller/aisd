package com.portifacto.aisd;

import org.testng.Assert;
import org.testng.annotations.Test;

public class AcyclicTest {

    @Test
    public void cyclesNone() {
        IGraph graph = new ArrayGraph();
        graph.addNode("Gdansk");
        graph.addNode("Sopot");
        graph.addNode("Gdynia");
        graph.addDirectedEdge("Gdansk", "Sopot");
        graph.addDirectedEdge("Sopot", "Gdynia");
        Acyclic acyclic = new Acyclic(graph);
        Assert.assertTrue(acyclic.isAcyclic());
    }

    @Test
    public void cyclesLarge() {
        IGraph graph = new ArrayGraph();
        graph.addNode("Gdansk");
        graph.addNode("Sopot");
        graph.addNode("Gdynia");
        graph.addDirectedEdge("Gdansk", "Sopot");
        graph.addDirectedEdge("Sopot", "Gdynia");
        graph.addDirectedEdge("Gdynia", "Gdansk");
        Acyclic acyclic = new Acyclic(graph);
        Assert.assertFalse(acyclic.isAcyclic());
    }

    @Test
    public void selfCycle() {
        IGraph graph = new ArrayGraph();
        graph.addNode("Gdansk");
        graph.addNode("Sopot");
        graph.addNode("Gdynia");
        graph.addDirectedEdge("Gdansk", "Gdansk");
        Acyclic acyclic = new Acyclic(graph);
        Assert.assertFalse(acyclic.isAcyclic());
    }

    @Test
    public void twoSeparateSubgraphs() {
        IGraph graph = new ArrayGraph();
        graph.addNode("0");
        graph.addNode("1");
        graph.addNode("2");
        graph.addNode("3");
        graph.addDirectedEdge("0", "1");
        graph.addDirectedEdge("2", "3");
        graph.addDirectedEdge("3", "2");
        Acyclic acyclic = new Acyclic(graph);
        Assert.assertFalse(acyclic.isAcyclic());
    }

    @Test
    public void largerGraphAcyclic() {
        IGraph graph = new ArrayGraph();
        graph.addNode("A");
        graph.addNode("B");
        graph.addNode("C");
        graph.addNode("D");
        graph.addNode("E");

        graph.addDirectedEdge("B", "A");
        graph.addDirectedEdge("B", "E");
        graph.addDirectedEdge("B", "D");
        graph.addDirectedEdge("C", "A");
        graph.addDirectedEdge("D", "C");
        graph.addDirectedEdge("E", "D");
        graph.addDirectedEdge("E", "A");

        Acyclic acyclic = new Acyclic(graph);
        Assert.assertTrue(acyclic.isAcyclic());
    }

}