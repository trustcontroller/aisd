package com.portifacto.aisd;

import org.testng.Assert;
import org.testng.annotations.Test;

public class HelperTest {

    @Test
    public void string() {
        double[] data = new double[5];
        data[0] = 22.2;
        data[1] = 33.3;
        String s = Helper.toString(data, 2);
        Assert.assertTrue(s.contains("22"));
        Assert.assertTrue(s.contains("33"));
    }

}