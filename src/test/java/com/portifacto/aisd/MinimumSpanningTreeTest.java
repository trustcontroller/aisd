package com.portifacto.aisd;

import org.testng.Assert;
import org.testng.annotations.Test;

public class MinimumSpanningTreeTest {

    @Test
    public void largeGraph() {
        ArrayGraph graph = new ArrayGraph();
        graph.addNode("0");
        graph.addNode("1");
        graph.addNode("2");
        graph.addNode("3");
        graph.addNode("4");
        graph.addNode("5");
        graph.addNode("6");
        graph.addNode("7");
        graph.addNode("8");

        graph.addBiDirectedEdge("0", "1", 4.0, 4.0);
        graph.addBiDirectedEdge("0", "7", 8.0, 8.0);
        graph.addBiDirectedEdge("1", "2", 8.0, 8.0);
        graph.addBiDirectedEdge("1", "7", 11.0, 11.0);
        graph.addBiDirectedEdge("2", "3", 7.0, 7.0);
        graph.addBiDirectedEdge("2", "5", 4.0, 4.0);
        graph.addBiDirectedEdge("2", "8", 2.0, 2.0);
        graph.addBiDirectedEdge("3", "4", 9.0, 9.0);
        graph.addBiDirectedEdge("3", "5", 14.0, 14.0);
        graph.addBiDirectedEdge("4", "5", 10.0, 10.0);
        graph.addBiDirectedEdge("5", "6", 2.0, 2.0);
        graph.addBiDirectedEdge("6", "7", 1.0, 1.0);
        graph.addBiDirectedEdge("6", "8", 6.0, 6.0);
        graph.addBiDirectedEdge("7", "8", 7.0, 7.0);

        MinimumSpanningTree mst = new MinimumSpanningTree(graph);
        Assert.assertTrue(mst.minimumSpanningTree());

        Assert.assertEquals(mst.getTree().length, 8);
        Assert.assertEquals(mst.getTree()[0].x, "0");
        Assert.assertEquals(mst.getTree()[0].y, "1");
        Assert.assertEquals(mst.getTree()[1].x, "1");
        Assert.assertEquals(mst.getTree()[1].y, "2");
        Assert.assertEquals(mst.getTree()[2].x, "2");
        Assert.assertEquals(mst.getTree()[2].y, "8");
        Assert.assertEquals(mst.getTree()[3].x, "2");
        Assert.assertEquals(mst.getTree()[3].y, "5");
        Assert.assertEquals(mst.getTree()[4].x, "5");
        Assert.assertEquals(mst.getTree()[4].y, "6");
        Assert.assertEquals(mst.getTree()[5].x, "6");
        Assert.assertEquals(mst.getTree()[5].y, "7");
        Assert.assertEquals(mst.getTree()[6].x, "2");
        Assert.assertEquals(mst.getTree()[6].y, "3");
        Assert.assertEquals(mst.getTree()[7].x, "3");
        Assert.assertEquals(mst.getTree()[7].y, "4");
        Assert.assertEquals(mst.getWeight(), 37);
    }

}