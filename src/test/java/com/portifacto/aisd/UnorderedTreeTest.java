package com.portifacto.aisd;

import org.testng.Assert;
import org.testng.annotations.Test;

public class UnorderedTreeTest extends TestHelper {

    @Test
    public void emptyTree() {
        UnorderedTree tree = new UnorderedTree();
        Assert.assertTrue(tree.isEmpty());
        Assert.assertTrue(identical(tree.iterator(),
                new double[]{}));
    }

    @Test
    public void rootOnly() {
        UnorderedTree tree = new UnorderedTree();
        tree.append(22.2);
        Assert.assertTrue(identical(tree.iterator(),
                new double[]{22.2}));
    }

    @Test
    public void rootAndTwoLeaves() {
        UnorderedTree tree = new UnorderedTree();
        Object root = tree.append(22.2);
        tree.append(root, 33.3);
        tree.append(root, 44.4);
        Assert.assertTrue(identical(tree.iterator(),
                new double[]{22.2, 33.3, 44.4}));
    }

    @Test
    public void threeLevels() {
        UnorderedTree tree = new UnorderedTree();
        Object root = tree.append(22.2);
        tree.append(root, 33.3);
        Object fortyFour = tree.append(root, 44.4);
        tree.append(fortyFour, 55.5);
        tree.append(fortyFour, 66.6);
        Assert.assertTrue(identical(tree.iterator(),
                new double[]{22.2, 33.3, 44.4, 55.5, 66.6}));
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void redefinedRoot() {
        UnorderedTree tree = new UnorderedTree();
        tree.append(22.2);
        tree.append(33.3);
        Assert.fail();
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void notTreeElement() {
        UnorderedTree tree = new UnorderedTree();
        tree.append(new Object(), 22.2);
        Assert.fail();
    }

}