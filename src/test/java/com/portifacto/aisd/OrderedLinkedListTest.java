package com.portifacto.aisd;

import org.testng.Assert;
import org.testng.annotations.Test;

public class OrderedLinkedListTest extends TestHelper {

    @Test
    public void nominalCase() {
        IList list = new OrderedLinkedList();
        list.add(10.0);
        list.add(20.0);
        list.add(15.0);
        list.add(5.0);
        Assert.assertTrue(identical(list, new double[]{5.0, 10.0, 15.0, 20.0}));
    }

    @Test
    public void emptyList() {
        IList list = new OrderedLinkedList();
        Assert.assertTrue(identical(list, new double[]{}));
        Assert.assertTrue(list.isEmpty());
        Assert.assertEquals(list.size(), 0);
        Assert.assertEquals(list.capacity(), Integer.MAX_VALUE);
    }

    @Test
    public void doubleValue() {
        IList list = new OrderedLinkedList();
        list.add(10.0);
        list.add(10.0);
        list.add(15.0);
        list.add(15.0);
        Assert.assertTrue(identical(list, new double[]{10.0, 15.0}));
    }

    @Test
    public void remove() {
        IList list = new OrderedLinkedList();
        list.add(10.0);
        list.add(20.0);
        list.add(15.0);
        list.add(5.0);
        list.add(25.0);
        list.remove(5.0);
        list.remove(25.0);
        list.remove(15.0);
        Assert.assertTrue(identical(list, new double[]{10.0, 20.0}));
    }

    @Test
    public void completeRemoval() {
        IList list = new OrderedLinkedList();
        list.add(10.0);
        list.add(20.0);
        list.remove(20.0);
        list.remove(10.0);
        Assert.assertTrue(identical(list, new double[]{}));
        Assert.assertTrue(list.isEmpty());

    }

    @Test
    public void find() {
        IList list = new OrderedLinkedList();
        list.add(10.0);
        list.add(25.0);
        Assert.assertTrue(list.find(10.0));
        Assert.assertTrue(list.find(25.0));
        Assert.assertFalse(list.find(0.0));
        Assert.assertFalse(list.find(20.0));
        Assert.assertFalse(list.find(30.0));
    }

    @Test
    public void removeFromTwoIdentical() {
        IList list = new OrderedLinkedList();
        list.add(10.0);
        list.add(10.0);
        list.remove(10.0);
        Assert.assertTrue(identical(list, new double[]{}));
    }


    @Test
    public void string() {
        IList list = new OrderedLinkedList();
        list.add(77.0);
        list.add(88.0);
        String s = list.toString();
        Assert.assertTrue(s.contains("77"));
        Assert.assertTrue(s.contains("88"));
    }


}