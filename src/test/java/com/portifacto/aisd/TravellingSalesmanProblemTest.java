package com.portifacto.aisd;

import org.testng.Assert;
import org.testng.annotations.Test;

public class TravellingSalesmanProblemTest {

    @Test
    public void basicCase() {
        ArrayGraph graph = new ArrayGraph();
        graph.addNode("A");
        graph.addNode("B");
        graph.addNode("C");
        graph.addNode("D");
        graph.addNode("E");

        graph.addUndirectedEdge("A", "B", 6.0);
        graph.addUndirectedEdge("A", "C", 5.0);
        graph.addUndirectedEdge("A", "D", 10.0);
        graph.addUndirectedEdge("A", "E", 15.0);
        graph.addUndirectedEdge("B", "C", 6.0);
        graph.addUndirectedEdge("B", "D", 14.0);
        graph.addUndirectedEdge("B", "E", 7.0);
        graph.addUndirectedEdge("C", "D", 7.0);
        graph.addUndirectedEdge("C", "E", 8.0);
        graph.addUndirectedEdge("D", "E", 7.0);

        TravellingSalesmanProblem tsp = new TravellingSalesmanProblem(graph, "A");
        double shortestDistance = tsp.calculate();
        String[] shortestPath = tsp.getMinPath();

        Assert.assertEquals(shortestDistance, 32.0);
        Assert.assertEquals(shortestPath.length, 6);

        String[] expected = new String[]{"A", "B", "E", "D", "C", "A"};
        for (int i = 0; i < expected.length; i++) {
            Assert.assertTrue(shortestPath[i].compareTo(expected[i]) == 0);
        }
    }

    @Test
    public void disconnectedGraph() {
        ArrayGraph graph = new ArrayGraph();
        graph.addNode("A");
        graph.addNode("B");
        graph.addNode("C");

        graph.addUndirectedEdge("A", "B", 6.0);

        TravellingSalesmanProblem tsp = new TravellingSalesmanProblem(graph, "A");
        double shortestDistance = tsp.calculate();
        Assert.assertEquals(shortestDistance, Double.POSITIVE_INFINITY);

    }
}