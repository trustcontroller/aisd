package com.portifacto.aisd;

import org.testng.Assert;
import org.testng.annotations.Test;

public class LinkedListTest extends TestHelper {

    @Test
    public void emptyList() {
        IList list = new LinkedList();
        Assert.assertTrue(list.isEmpty());
    }

    @Test
    public void findOnEmptyList() {
        IList list = new LinkedList();
        boolean found = list.find(12.0);
        Assert.assertFalse(found);
    }

    @Test
    public void findExisting() {
        IList list = new LinkedList();
        list.add(10.0);
        boolean found = list.find(10.0);
        Assert.assertTrue(found);
    }

    @Test
    public void findNotExisting() {
        IList list = new LinkedList();
        list.add(10.0);
        boolean found = list.find(11.0);
        Assert.assertFalse(found);
    }

    @Test
    public void removeCentral() {
        IList list = new LinkedList();
        list.add(10.0);
        list.add(20.0);
        list.add(30.0);

        list.remove(20.0);

        Assert.assertTrue(identical(list, new double[]{10.0, 30.0}));
    }

    @Test
    public void removeLast() {
        IList list = new LinkedList();
        list.add(10.0);
        list.add(20.0);
        list.add(30.0);

        list.remove(10.0);

        Assert.assertTrue(identical(list, new double[]{20.0, 30.0}));
    }

    @Test
    public void removeFirst() {
        IList list = new LinkedList();
        list.add(10.0);
        list.add(20.0);
        list.add(30.0);

        list.remove(30.0);

        Assert.assertTrue(identical(list, new double[]{10.0, 20.0}));
    }

    @Test
    public void twoIdenticalValues() {
        IList list = new LinkedList();
        list.add(10.0);
        list.add(10.0);

        Assert.assertTrue(identical(list, new double[]{10.0, 10.0}));
    }

    @Test
    public void removeAnyOfIdentical() {
        IList list = new LinkedList();
        list.add(10.0);
        list.add(15.0);
        list.add(10.0);
        list.remove(10);

        Assert.assertTrue(list.find(10.0));
        Assert.assertTrue(list.find(15.0));
    }

    @Test
    public void string() {
        IList list = new LinkedList();
        list.add(77.0);
        list.add(88.0);
        String s = list.toString();
        Assert.assertTrue(s.contains("77"));
        Assert.assertTrue(s.contains("88"));
    }

    @Test
    public void threadSafeAdd() {
        LinkedList list = new LinkedList();
        list.threadSafeAdd(22.0);
        list.threadSafeAdd(33.0);
        Assert.assertTrue(list.find(33.0));
    }

    @Test
    public void recursiveFind() {
        LinkedList list = new LinkedList();
        list.add(22.0);
        list.add(33.0);
        Assert.assertTrue(list.findRecursive(33.0));
    }

    @Test
    public void recursiveFindNonExisting() {
        LinkedList list = new LinkedList();
        list.add(22.0);
        list.add(33.0);
        Assert.assertFalse(list.findRecursive(44.0));
    }


}