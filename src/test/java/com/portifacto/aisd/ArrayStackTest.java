package com.portifacto.aisd;

import org.testng.Assert;
import org.testng.annotations.Test;

public class ArrayStackTest {

    private static final double EPSILON = 0.0001;

    @Test
    public void empty() {
        ArrayStack arrayStack = new ArrayStack();

        Double result = arrayStack.pull();
        Assert.assertNull(result);
    }

    @Test
    public void expandedStack() {
        ArrayStack arrayStack = new ArrayStack(3);
        arrayStack.push(10.0);
        arrayStack.push(20.0);
        arrayStack.push(30.0);
        arrayStack.push(40.0);
        Assert.assertEquals(arrayStack.pull().doubleValue(), 40.0, EPSILON);
        Assert.assertEquals(arrayStack.pull().doubleValue(), 30.0, EPSILON);
        Assert.assertEquals(arrayStack.pull().doubleValue(), 20.0, EPSILON);
        Assert.assertEquals(arrayStack.pull().doubleValue(), 10.0, EPSILON);
        Assert.assertNull(arrayStack.pull());
    }

    @Test
    public void pushAndPull() {
        ArrayStack arrayStack = new ArrayStack();

        arrayStack.push(20.0);
        Double result = arrayStack.pull();
        Assert.assertNotNull(result);
        Assert.assertEquals(20.0, result.doubleValue(), EPSILON);
    }

    @Test
    public void twoPushesAndPulls() {
        ArrayStack arrayStack = new ArrayStack();

        arrayStack.push(20.0);
        arrayStack.push(30.0);
        Double r1 = arrayStack.pull();
        Double r2 = arrayStack.pull();
        Assert.assertNotNull(r1);
        Assert.assertNotNull(r2);
        Assert.assertEquals(30.0, r1.doubleValue(), EPSILON);
        Assert.assertEquals(20.0, r2.doubleValue(), EPSILON);
    }

}
