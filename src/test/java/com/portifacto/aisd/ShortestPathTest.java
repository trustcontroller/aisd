package com.portifacto.aisd;

import org.testng.Assert;
import org.testng.annotations.Test;

public class ShortestPathTest extends TestHelper {

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void nullGraph() {
        ShortestPath shortestPath = new ShortestPath(null);
        Assert.fail();
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void nullNames() {
        IGraph graph = new ArrayGraph();
        ShortestPath shortestPath = new ShortestPath(graph);
        shortestPath.shortestPath(null, null);
        Assert.fail();
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void nonExistingNodes() {
        IGraph graph = new ArrayGraph();
        ShortestPath shortestPath = new ShortestPath(graph);
        shortestPath.shortestPath("A", "B");
        Assert.fail();
    }

    @Test
    public void shortestPathFound() {
        IGraph graph = new ArrayGraph();
        graph.addNode("1");
        graph.addNode("2");
        graph.addNode("3");
        graph.addNode("4");
        graph.addNode("5");
        graph.addNode("6");
        graph.addBiDirectedEdge("1", "2", 7.0, 7.0);
        graph.addBiDirectedEdge("1", "3", 9.0, 9.0);
        graph.addBiDirectedEdge("1", "6", 14.0, 14.0);
        graph.addBiDirectedEdge("2", "3", 10.0, 10.0);
        graph.addBiDirectedEdge("2", "4", 15.0, 15.0);
        graph.addBiDirectedEdge("3", "4", 11.0, 11.0);
        graph.addBiDirectedEdge("3", "6", 2.0, 2.0);
        graph.addBiDirectedEdge("4", "5", 6.0, 6.0);
        graph.addBiDirectedEdge("5", "6", 9.0, 9.0);
        ShortestPath shortestPath = new ShortestPath(graph);
        Assert.assertTrue(shortestPath.shortestPath("1", "5"));
        Assert.assertEquals(shortestPath.getDistance(), 20.0);
        Assert.assertTrue(identical(shortestPath.getPath(), new String[]{"1", "3", "6", "5"}));
    }

    @Test
    public void shortestPathNotFound() {
        IGraph graph = new ArrayGraph();
        graph.addNode("1");
        graph.addNode("2");
        ShortestPath shortestPath = new ShortestPath(graph);
        Assert.assertFalse(shortestPath.shortestPath("1", "2"));
        Assert.assertEquals(shortestPath.getDistance(), Double.POSITIVE_INFINITY);
    }


}