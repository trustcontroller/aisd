package com.portifacto.aisd;

import org.testng.Assert;
import org.testng.annotations.Test;

public class PigeonholeSortTest extends TestHelper {

    @Test
    public void orderPreserved() {
        int[] vector = {2, 4, 1, 3};
        int[] result = new PigeonholeSort().sort(vector);
        Assert.assertTrue(identical(result, new int[]{1, 2, 3, 4}));
    }

    @Test
    public void zeroLength() {
        int[] vector = {};
        int[] result = new PigeonholeSort().sort(vector);
        Assert.assertTrue(identical(result, new int[]{}));
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void nullInput() {
        int[] vector = null;
        int[] result = new PigeonholeSort().sort(vector);
        Assert.fail();
    }

}