package com.portifacto.aisd;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.concurrent.LinkedBlockingQueue;

public class LinkedQueueTest {

    @Test
    public void empty() {
        LinkedQueue queue = new LinkedQueue();
        Assert.assertNull(queue.poll());
        Assert.assertEquals(queue.size(), 0);
        Assert.assertTrue(queue.isEmpty());
        Assert.assertEquals(queue.capacity(), Integer.MAX_VALUE);
    }

    @Test
    public void overloaded() {
        LinkedQueue queue = new LinkedQueue(2);
        queue.offer(1.0);
        queue.offer(2.0);
        Assert.assertFalse(queue.offer(3.0));
    }

    @Test
    public void addOne() {
        LinkedQueue queue = new LinkedQueue(5);
        Assert.assertTrue(queue.offer(5.0));
    }

    @Test
    public void orderOfRetrieval() {
        LinkedQueue queue = new LinkedQueue(10);
        queue.offer(1.0);
        queue.offer(2.0);
        Double v1 = queue.poll();
        Double v2 = queue.poll();
        Assert.assertNotNull(v1);
        Assert.assertNotNull(v2);
        Assert.assertEquals(v1.doubleValue(), 1.0);
        Assert.assertEquals(v2.doubleValue(), 2.0);
    }

    @Test
    public void leadsToEmptyQueue() {
        LinkedQueue queue = new LinkedQueue();
        queue.offer(1.0);
        queue.poll();
        Assert.assertNull(queue.poll());
    }


    private class Task {

    }

    public void exampleOfSynchronization1() {

        try {


            LinkedBlockingQueue<Task> queue = new LinkedBlockingQueue<>();


            // producer
            while (true) {
                // ...
                queue.put(new Task()); // waits if the queue is full
                // ...
            }

        } catch (Exception e) {
        }
    }

    public void exampleOfSynchronization2() {

        LinkedBlockingQueue<Task> queue = new LinkedBlockingQueue<>();

        try {


            // consumer
            while (true) {
                // ...
                Task task = queue.take(); // waits if empty
            }

        } catch (Exception e) {
        }
    }


}