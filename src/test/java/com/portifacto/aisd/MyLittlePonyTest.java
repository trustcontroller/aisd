package com.portifacto.aisd;

import org.testng.Assert;
import org.testng.annotations.Test;

public class MyLittlePonyTest {

    @Test
    public void twoPoniesNotEqual() {
        MyLittlePony pony1 = new MyLittlePony(MyLittlePony.Colour.GREEN);
        MyLittlePony pony2 = new MyLittlePony(MyLittlePony.Colour.GREEN);
        Assert.assertFalse(pony1.equals(pony2));
    }

    @Test
    public void ponyIsNotDouble() {
        MyLittlePony pony1 = new MyLittlePony(MyLittlePony.Colour.GREEN);
        Double d1 = 22.2;
        Assert.assertFalse(pony1.equals(d1));
    }

    @Test
    public void twoPoniesHash() {
        MyLittlePony pony1 = new MyLittlePony(MyLittlePony.Colour.GREEN);
        MyLittlePony pony2 = new MyLittlePony(MyLittlePony.Colour.GREEN);
        Assert.assertFalse(pony1.hashCode() == pony2.hashCode());
    }

    @Test
    public void twoPoniesSameColour() {
        MyLittlePony pony1 = new MyLittlePony(MyLittlePony.Colour.GREEN);
        MyLittlePony pony2 = new MyLittlePony(MyLittlePony.Colour.GREEN);
        Assert.assertEquals(pony1.compareTo(pony2), 0);
    }

    @Test
    public void twoPoniesDifferentColour() {
        MyLittlePony pony1 = new MyLittlePony(MyLittlePony.Colour.GREEN);
        MyLittlePony pony2 = new MyLittlePony(MyLittlePony.Colour.RAINBOW);
        Assert.assertTrue(pony1.compareTo(pony2) != 0);
    }

}