package com.portifacto.aisd;

import org.testng.Assert;
import org.testng.annotations.Test;

public class UnorderedBinaryTreeTest extends TestHelper {

    @Test
    public void emptyTree() {
        UnorderedBinaryTree tree = new UnorderedBinaryTree();
        Assert.assertTrue(tree.isEmpty());
        Assert.assertTrue(identical(tree.iterator(),
                new double[]{}));
    }

    @Test
    public void rootOnly() {
        UnorderedBinaryTree tree = new UnorderedBinaryTree();
        tree.append(22.2);
        Assert.assertTrue(identical(tree.iterator(),
                new double[]{22.2}));
    }

    @Test
    public void rootAndTwoLeaves() {
        UnorderedBinaryTree tree = new UnorderedBinaryTree();
        Object root = tree.append(22.2);
        tree.appendLeft(root, 33.3);
        tree.appendRight(root, 44.4);
        Assert.assertTrue(identical(tree.iterator(),
                new double[]{22.2, 33.3, 44.4}));
    }

    @Test
    public void threeLevels() {
        UnorderedBinaryTree tree = new UnorderedBinaryTree();
        Object root = tree.append(22.2);
        Object threeThree = tree.appendLeft(root, 33.3);
        Object fortyFour = tree.appendRight(root, 44.4);
        tree.appendLeft(threeThree, 11.1);
        tree.appendLeft(fortyFour, 55.5);
        tree.appendRight(fortyFour, 66.6);
        Assert.assertTrue(identical(tree.iterator(),
                new double[]{22.2, 33.3, 11.1, 44.4, 55.5, 66.6}));
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void redefinedRoot() {
        UnorderedBinaryTree tree = new UnorderedBinaryTree();
        tree.append(22.2);
        tree.append(33.3);
        Assert.fail();
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void notTreeElementLeft() {
        UnorderedBinaryTree tree = new UnorderedBinaryTree();
        tree.appendLeft(new Object(), 22.2);
        Assert.fail();
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void notTreeElementRight() {
        UnorderedBinaryTree tree = new UnorderedBinaryTree();
        tree.appendRight(new Object(), 22.2);
        Assert.fail();
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void redefineLeft() {
        UnorderedBinaryTree tree = new UnorderedBinaryTree();
        Object root = tree.append(22.2);
        tree.appendLeft(root, 33.3);
        tree.appendLeft(root, 44.4);
        Assert.fail();
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void redefineRight() {
        UnorderedBinaryTree tree = new UnorderedBinaryTree();
        Object root = tree.append(22.2);
        tree.appendRight(root, 33.3);
        tree.appendRight(root, 44.4);
        Assert.fail();
    }


}