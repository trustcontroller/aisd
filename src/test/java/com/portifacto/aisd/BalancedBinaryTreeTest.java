package com.portifacto.aisd;

import org.testng.Assert;
import org.testng.annotations.Test;

public class BalancedBinaryTreeTest extends TestHelper {

    @Test
    public void empty() {
        BalancedBinaryOrderedTree tree = new BalancedBinaryOrderedTree();
        Assert.assertEquals(tree.height(), 0);
    }

    @Test
    public void threeInOrder() {
        BalancedBinaryOrderedTree tree = new BalancedBinaryOrderedTree();
        tree.add(1.0);
        tree.add(2.0);
        tree.add(3.0);
        Assert.assertEquals(tree.height(), 1);
        Assert.assertTrue(identical(tree.iterator(),
                new double[]{1.0, 2.0, 3.0}));
    }

    @Test
    public void sevenPacked() {
        BalancedBinaryOrderedTree tree = new BalancedBinaryOrderedTree();
        tree.add(1.0);
        tree.add(6.0);
        tree.add(5.0);
        tree.add(2.0);
        tree.add(7.0);
        tree.add(3.0);
        tree.add(4.0);
        Assert.assertEquals(tree.height(), 3);
        Assert.assertTrue(identical(tree.iterator(),
                new double[]{1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0}));
    }

    @Test
    public void theSameElement() {
        BalancedBinaryOrderedTree tree = new BalancedBinaryOrderedTree();
        tree.add(6.0);
        tree.add(6.0);
        Assert.assertTrue(identical(tree.iterator(),
                new double[]{6.0}));
    }

    @Test
    public void leftAddToRoot() {
        BalancedBinaryOrderedTree tree = new BalancedBinaryOrderedTree();
        tree.add(6.0);
        tree.add(4.0);
        Assert.assertTrue(identical(tree.iterator(),
                new double[]{4.0, 6.0}));
    }

    @Test
    public void rightAddToRoot() {
        BalancedBinaryOrderedTree tree = new BalancedBinaryOrderedTree();
        tree.add(6.0);
        tree.add(8.0);
        Assert.assertTrue(identical(tree.iterator(),
                new double[]{6.0, 8.0}));
    }

    @Test
    public void cascadingAddLeftLeft() {
        BalancedBinaryOrderedTree tree = new BalancedBinaryOrderedTree();
        tree.add(6.0);
        tree.add(4.0);
        tree.add(2.0);
        Assert.assertTrue(identical(tree.iterator(),
                new double[]{2.0, 4.0, 6.0}));
    }

    @Test
    public void cascadingAddLeftRight() {
        BalancedBinaryOrderedTree tree = new BalancedBinaryOrderedTree();
        tree.add(6.0);
        tree.add(4.0);
        tree.add(5.0);
        Assert.assertTrue(identical(tree.iterator(),
                new double[]{4.0, 5.0, 6.0}));
    }

    @Test
    public void cascadingAddRightLeft() {
        BalancedBinaryOrderedTree tree = new BalancedBinaryOrderedTree();
        tree.add(6.0);
        tree.add(8.0);
        tree.add(7.0);
        Assert.assertTrue(identical(tree.iterator(),
                new double[]{6.0, 7.0, 8.0}));
    }

    @Test
    public void cascadingAddRightRight() {
        BalancedBinaryOrderedTree tree = new BalancedBinaryOrderedTree();
        tree.add(6.0);
        tree.add(8.0);
        tree.add(9.0);
        Assert.assertTrue(identical(tree.iterator(),
                new double[]{6.0, 8.0, 9.0}));
    }

    @Test
    public void repeatedValueAtRoot() {
        BalancedBinaryOrderedTree tree = new BalancedBinaryOrderedTree();
        Assert.assertTrue(tree.add(6.0));
        Assert.assertFalse(tree.add(6.0));
    }

    @Test
    public void repeatedValueAtFirstLevel() {
        BalancedBinaryOrderedTree tree = new BalancedBinaryOrderedTree();
        Assert.assertTrue(tree.add(6.0));
        Assert.assertTrue(tree.add(7.0));
        Assert.assertFalse(tree.add(7.0));
    }
}