package com.portifacto.aisd;

import org.testng.Assert;
import org.testng.annotations.Test;

public class HashBucketTest {

    @Test
    public void capacity() {
        ISet hashSet = new HashBucket();
        Assert.assertEquals(hashSet.capacity(), Integer.MAX_VALUE);
    }

    @Test
    public void findInEmpty() {
        ISet hashSet = new HashBucket();
        Assert.assertFalse(hashSet.find(2.0));
    }

    @Test
    public void findInOneElement() {
        ISet hashSet = new HashBucket();
        hashSet.add(2.0);
        Assert.assertTrue(hashSet.find(2.0));
    }

    @Test
    public void iteratorOnEmpty() {
        ISet hashSet = new HashBucket();
        IIterator iterator = hashSet.iterator();
        Double value = iterator.get();
        Assert.assertNull(value);
    }

    @Test
    public void iteratorOnOne() {
        HashSet hashSet = new HashSet();
        hashSet.add(22.2);
        IIterator iterator = hashSet.iterator();
        Double value = iterator.get();
        Assert.assertEquals(value.doubleValue(), 22.2, 0.001);
    }

    @Test
    public void iteratorOnTwo() {
        HashSet hashSet = new HashSet();
        hashSet.add(22.0);
        hashSet.add(33.0);
        IIterator iterator = hashSet.iterator();
        Double v1 = iterator.get();
        Double v2 = iterator.get();
        Double v3 = iterator.get();
        Assert.assertTrue(v1.doubleValue() == 22.0 || v2.doubleValue() == 22.0);
        Assert.assertTrue(v1.doubleValue() == 33.0 || v2.doubleValue() == 33.0);
        Assert.assertNull(v3);
    }


}