package com.portifacto.aisd;

import org.testng.Assert;
import org.testng.annotations.Test;

public class HierarchicalQueueListTest {

    @Test
    public void empty() {
        HierarchicalQueueList queue = new HierarchicalQueueList();
        Assert.assertNull(queue.poll());
        Assert.assertEquals(queue.used(), 0);
        Assert.assertEquals(queue.capacity(), Integer.MAX_VALUE);
    }

    @Test
    public void oneWithDefaultPriority() {
        HierarchicalQueueList queue = new HierarchicalQueueList();
        queue.offer(10.0);
        Double result = queue.poll();
        Assert.assertNotNull(result);
        Assert.assertEquals(10.0, result.doubleValue());
    }

    @Test
    public void oneAdded() {
        HierarchicalQueueList queue = new HierarchicalQueueList();
        queue.offer(10.0, 2);
        Double result = queue.poll();
        Assert.assertNotNull(result);
        Assert.assertEquals(10.0, result.doubleValue());
    }

    @Test
    public void threePriorities() {
        HierarchicalQueueList queue = new HierarchicalQueueList();
        queue.offer(22.0, 2);
        queue.offer(44.0, 4);
        queue.offer(33.0, 3);
        Double result = queue.poll();
        Assert.assertNotNull(result);
        Assert.assertEquals(result.doubleValue(), 44);
    }

    @Test
    public void oneAddedAndRemoved() {
        HierarchicalQueueList queue = new HierarchicalQueueList();
        queue.offer(10.0, 2);
        queue.poll();
        Double result = queue.poll();
        Assert.assertNull(result);
    }

    @Test
    public void twoSamePriority() {
        HierarchicalQueueList queue = new HierarchicalQueueList();
        queue.offer(10.0, 2);
        queue.offer(20.0, 2);
        Double r1 = queue.poll();
        Double r2 = queue.poll();
        Assert.assertNotNull(r1);
        Assert.assertNotNull(r2);
        Assert.assertEquals(r1.doubleValue(), 10.0);
        Assert.assertEquals(r2.doubleValue(), 20.0);

    }

    @Test
    public void twoDifferentPriority() {
        HierarchicalQueueList queue = new HierarchicalQueueList();
        queue.offer(10.0, 1);
        queue.offer(20.0, 2);
        Double r1 = queue.poll();
        Double r2 = queue.poll();
        Assert.assertNotNull(r1);
        Assert.assertNotNull(r2);
        Assert.assertEquals(r1.doubleValue(), 20.0);
        Assert.assertEquals(r2.doubleValue(), 10.0);
    }

    @Test
    public void sample() {
        IHierarchicalQueue queue = new HierarchicalQueueList();
        queue.offer(14.0, 10);
        queue.offer(11.0, 5);
        queue.offer(13.0, 10);
    }

}